﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PathFollower))]
public class Resource : MonoBehaviour {

	public bool isTribute {get; set;}

	private Settlement currentLocation;

	//this should only be changed wrt tribute stockpiling
	public int value = 1;

	public bool isHeld = false;

	public ResourceData data;

	public SpriteRenderer iconRenderer;

	/*[SerializeField]
	public List<ResourceSprite> tradeSprites;

	[SerializeField]
	public List<ResourceSprite> tributeSprites;*/

	public enum ResourceType
	{
		None,
		Bread,
		Herbs,
		Tools,
		Yarn
	}

	public void Awake()
	{
		GetComponent<PathFollower>().OnReachDestinationMessage = Messages.Events.ResourceArrived;
	}

	public void OnReachDestination()
	{
		if (!isHeld)
			Destroy(gameObject);
	}

	/*public Sprite GetSprite(ResourceType type, bool tribute = false)
	{
		if (tribute)
		{
		foreach (ResourceSprite sprite in tributeSprites)
			{
				if (sprite.type == type)
				{
					return sprite.sprite.GetComponent<SpriteRenderer>().sprite;
				}

			}
		}
		else
		{
			foreach (ResourceSprite sprite in tradeSprites)
			{
				if (sprite.type == type)
				{
					return sprite.sprite.GetComponent<SpriteRenderer>().sprite;
				}

			}
		}

		return null;
	}*/

	public void SetActiveSprite()
	{
		iconRenderer.sprite = data.GetSpriteByType(type, isTribute);


		/*if (isTribute)
		{
			foreach (ResourceSprite sprite in tributeSprites)
			{
				if (sprite.type == type)
				{
					sprite.sprite.SetActive(true);
				}
				else
				{
					sprite.sprite.SetActive(false);
				}
				
			}

			foreach(ResourceSprite sprite in tradeSprites)
			{
				sprite.sprite.SetActive(false);
			}
		}
		else
		{
			foreach (ResourceSprite sprite in tradeSprites)
			{
				if (sprite.type == type)
				{
					sprite.sprite.SetActive(true);
				}
				else
				{
					sprite.sprite.SetActive(false);
				}

			}

			foreach(ResourceSprite sprite in tributeSprites)
			{
				sprite.sprite.SetActive(false);
			}
		}*/
	}

	public ResourceType type = ResourceType.Bread;


	/*public float quantity;

	public void Setup(resourceType _type, bool tribute, Settlement dest)
	{
		type = _type;
		isTribute = tribute;

		if (isTribute)
		{
			GetComponent<MeshRenderer>().material.color = new Color(255, 197, 45);
		}

		destination = dest;

		StartCoroutine(MoveToDestination());

	}

	IEnumerator MoveToDestination()
	{
		while (Vector3.Distance(transform.position, destination.transform.position) > 0.01f)
		{
			transform.position = Vector3.MoveTowards(transform.position, destination.transform.position, moveSpeed * Time.deltaTime);
			
			
			yield return 0;
		}
		transform.position = destination.transform.position;
	}
		
	// Update is called once per frame
	void Update () {
	
	}*/
}
