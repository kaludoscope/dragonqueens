﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class PathFollower : MonoBehaviour {

	private LinkedList<PathNode> currentRoute = new LinkedList<PathNode>();

	private LinkedListNode<PathNode> currentNode;

	public Enum OnReachDestinationMessage = null;

	public float speed = 0.3f;
	 
	public Resource res = null;

	public void SetRoute(LinkedList<PathNode> newRoute)
	{
		currentRoute = newRoute;

		if (currentRoute.Count > 0)
			currentNode = currentRoute.Last;
	}

	public void SetRoute(Route newRoute)
	{
		SetRoute(newRoute.nodes);
	}

	void Awake()
	{
		res = GetComponent<Resource>();


		currentRoute = new LinkedList<PathNode>();

	}

	// Update is called once per frame
	void Update () {
	
		if (currentRoute.Count == 0 || currentNode == null)
		{
			return;
		}

		if (Vector3.Distance(transform.position, currentNode.Value.transform.position) > 0.01f)
		{
			transform.position = Vector3.MoveTowards(transform.position, currentNode.Value.transform.position, speed);	
		}
		else
		{
			if (currentNode == currentRoute.First)
			{
				currentNode = null;

				if (res != null)
					res.OnReachDestination();
					
				if (OnReachDestinationMessage != null)
					Game.instance.messageBroker.SendMessage(OnReachDestinationMessage);
			}
			else
			{
				transform.position = currentNode.Value.transform.position;

				currentNode = currentNode.Previous;
			}
		}

	}


}
