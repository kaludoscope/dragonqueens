﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

public class PathNode : MonoBehaviour
{

	public enum Type
	{
		Main,
		Sub,
		Pawn
	}

	public List<Unit> units = new List<Unit>();

	protected List<Unit> multiUnits;


	public List<PathNode> connectedNodes = new List<PathNode>();
	public List<PathSection> connectedSections = new List<PathSection>();

	public Structure connectedStructure = null;
	public SpriteRenderer spriteRenderer = null;

	public bool available {get; set;}

	public Type type = Type.Main;

	public virtual void Awake()
	{
		available = false;
		connectedStructure = GetComponent<Structure>();
		spriteRenderer = GetComponentInChildren<SpriteRenderer>();

		Game.instance.messageBroker.SubscribeTo<Structure>(Messages.Events.SettlementSelected, UnHighlight);

		Game.instance.messageBroker.SubscribeTo(Messages.Events.ClearAllHighlights, UnHighlight);
	}

	public void Highlight(bool includeCastles, bool includePawns)
	{
		if (includePawns)
		{
			if (type == Type.Pawn)
			{
				if (spriteRenderer != null)
					spriteRenderer.enabled = true;

				available = true;
			}
		}
		else if (connectedStructure != null)
		{
			if (!includeCastles && connectedStructure is Castle) return;

			connectedStructure.ToggleHighlight(true);
			available = true;

		}
	}

	public void UnHighlight(Structure s = null)
	{
		if (s == connectedStructure) return;

		if (connectedStructure != null)
		{
			connectedStructure.ToggleHighlight(false);
		}
		else if (spriteRenderer != null)
		{ 
			spriteRenderer.enabled = false;
		}

		available = false;
	}

	public void UnHighlight()
	{
		if (connectedStructure != null)
		{
			connectedStructure.ToggleHighlight(false);
		}
		else if (spriteRenderer != null)
		{ 
			spriteRenderer.enabled = false;
		}

		available = false;
	}

	public bool Blocked(Dragon queen)
	{
		bool isBlocked = false;

		switch (type)
		{
		case Type.Main:
			
			isBlocked = !CanPassThrough(queen);
			break;

		case Type.Pawn:
			
			if (units.Count > 0)
			{
				foreach (Unit p in units)
				{
					if (p is Pawn && p.queen != queen)
					{
						isBlocked = true;
						break;
					}
				}
			}

			break;

		default:
			isBlocked = false;
			break;

		}

		return isBlocked;

	}

	public virtual void OnMouseUpAsButton()
	{
		if (!EventSystem.current.IsPointerOverGameObject() && !EventSystem.current.IsPointerOverGameObject(0))
		{
			Game.instance.messageBroker.SendMessage<PathNode>(Messages.Input.PathNodeClicked, this);
		}
	}


	public bool CanPassThrough(Dragon forQueen)
	{
		if (connectedStructure == null) return true;

		bool passThrough = (connectedStructure.queen == forQueen) || (connectedStructure is Town);

		if (!passThrough)
		{
			foreach (Unit u in units)
			{
				if (u.queen == forQueen)
					passThrough = true;
				break;
			}
		}

		//Debug.Log("Pass through " + passThrough, this);

		return passThrough;
	}

	public virtual Unit SelectCurrentUnit(UnitData.UnitType unitType)
	{
		Unit selectedUnit = null;

		foreach (Unit u in units)
		{
			if (u.UnitType == unitType && !u.hasMoved)
			{
				selectedUnit = u;
				break;
			}
		}

		if (connectedStructure == null) return selectedUnit;

		if (connectedStructure.canBuildUnits && selectedUnit == null)
		{
			selectedUnit = BuildUnit(unitType);
		}

		Debug.Assert(selectedUnit != null);

		return selectedUnit;
	}

	public Unit BuildUnit(UnitData.UnitType unitType)
	{

		Unit newUnit = connectedStructure.BuildUnit(unitType);

		units.Add(newUnit);

		return newUnit;

	}

	public virtual void AddUnit(Unit u)
	{
		Debug.Assert(!units.Contains(u));

		if (connectedStructure != null)
		{
			
			connectedStructure.AddUnit(u);
		}

		units.Add(u);
	}

	public virtual void RemoveUnit(Unit u)
	{
		Debug.Assert(units.Contains(u));

		units.Remove(u);
	}

	public bool HasMultipleUnits(Unit matchUnit)
	{
		multiUnits = new List<Unit>();

		foreach (Unit u in units)
		{
			if (u.UnitType == matchUnit.UnitType && 
				!u.hasMoved &&
				u.queen == matchUnit.queen)
			{
				multiUnits.Add(u);
			}
		}

		return multiUnits.Count > 1;
	}
		
	public Unit[] GetUnits(Unit matchUnit, int n)
	{
		if (!multiUnits.Contains(matchUnit))
		{
			multiUnits = new List<Unit>();

			foreach (Unit u in units)
			{
				if (u.UnitType == matchUnit.UnitType && 
					!u.hasMoved &&
					u.queen == matchUnit.queen)
				{
					multiUnits.Add(u);
				}
			}
		}

		Unit[] tempUnits = new Unit[n];
		multiUnits.CopyTo(0, tempUnits, 0, n);

		return tempUnits;
	}

	public int numAvailableUnits(Unit matchUnit)
	{
		HasMultipleUnits(matchUnit);

		return multiUnits.Count;
	}

}

