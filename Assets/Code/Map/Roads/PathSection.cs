﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathSection: MonoBehaviour
{
	public PathNode [] nodes;
	public LineRenderer lineRenderer;

	public List<Pawn> pawns = new List<Pawn>();

	public PathNode pawnNode {get; private set;}

	public Texture roadBase;

	public void Init(PathNode a, PathNode b, LineRenderer r = null)
	{
		nodes = new PathNode[2];

		nodes[0] = a;
		nodes[1] = b;

		lineRenderer = r;

	}

	public bool HasNodes(PathNode a, PathNode b)
	{
		return ((nodes[0] == a && nodes[1] == b) 
			|| (nodes [0] == b && nodes[1] == a));
	}

	public bool HasNodes(PathNode[] checkNodes)
	{
		return ((nodes[0] == checkNodes[0] && nodes[1] == checkNodes[1]) 
			|| (nodes [0] == checkNodes[1] && nodes[1] == checkNodes[0]));
	}

}

