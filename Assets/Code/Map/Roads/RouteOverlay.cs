﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(LineRenderer))]

public class RouteOverlay : MonoBehaviour
{
	private LineRenderer lineRenderer;

	[SerializeField]
	private Texture chevrons;
	[SerializeField]
	private Texture blockade;

	public Route currentRoute;

	public void Awake()
	{
		lineRenderer = GetComponent<LineRenderer>();

		lineRenderer.sortingLayerName = "Overlay";

		Game.instance.messageBroker.SubscribeTo<Route>(Messages.UI.UnhighlightRoute, UnhighlightRoute);
		Game.instance.messageBroker.SubscribeTo(Messages.Events.ClearAllHighlights, UnhighlightRoute);
	}

	public void HighlightRoute(Route r)
	{
		currentRoute = r;

		lineRenderer.numPositions = r.nodes.Count;

		var i= 0;

		for (LinkedListNode<PathNode> node = r.nodes.First; node != null; node = node.Next)
		{
			lineRenderer.SetPosition(i, node.Value.transform.position);

			i++;
		}

		switch (r.currentDisplayMode)
		{
		case Route.DisplayMode.TradeRoute:
			lineRenderer.material.mainTexture = chevrons;
			break;

		case Route.DisplayMode.Blockaded:
			lineRenderer.material.mainTexture = blockade;
			break;

		case Route.DisplayMode.Selected:
			lineRenderer.material.mainTexture = null;
			break;
		}

		lineRenderer.enabled = true;
	}

	public void UnhighlightRoute(Route r)
	{
		if (r == currentRoute)
		{
			UnhighlightRoute();
		}
	}

	public void UnhighlightRoute()
	{
		if (currentRoute == null) return;

		lineRenderer.enabled = false;

		currentRoute.currentDisplayMode = Route.DisplayMode.Normal;

		currentRoute = null;

	}

}

