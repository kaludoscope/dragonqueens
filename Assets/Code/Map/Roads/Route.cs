﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Route
{
	public LinkedList<PathNode> nodes;
	List<PathSection> sections;

	/*private GameObject overlay;
	private LineRenderer overlayRenderer;*/



	public enum DisplayMode
	{
		Normal,
		TradeRoute,
		Blockaded,
		Selected
	}

	public DisplayMode currentDisplayMode = DisplayMode.Normal;

	public Route(LinkedList<PathNode> _nodes)
	{
		nodes = _nodes;

		sections = new List<PathSection>();

		//overlay = Instantiate<GameObject>(Map.RoutePrefab, nodes.First.Value.transform.parent);


	}

	/*public void SetupOverlay(GameObject prefab, Transform parent)
	{
		overlay = Instantiate<GameObject>(prefab, parent);

		overlayRenderer = overlay.GetComponent<LineRenderer>();

		overlayRenderer.numPositions = nodes.Count;

		var i= 0;

		for (LinkedListNode<PathNode> node = nodes.First; node != nodes.Last; node = node.Next)
		{
			overlayRenderer.SetPosition(i, node.Value.transform.position);


			i++;
		}

	}*/

	public bool IsBlockadedFor(Dragon queen)
	{
		bool isBlockaded = false;

		for (LinkedListNode<PathNode> node = nodes.First; node != nodes.Last; node = node.Next)
		{
			
			if (node.Value.Blocked(queen) && node != nodes.First)
			{
				isBlockaded = true;
			}
			//sections.Add(node.Value.connectedSections.Find(x => x.HasNodes(node.Value, node.Next.Value)));



		}

		/*foreach (PathSection sec in sections)
		{
			if (sec.IsBlockadedFor(queen))
			{
				isBlockaded = true;
				break;
			}
		}*/

		return isBlockaded;
	}

	/* display modes:
	 * - normal
	 * - as trade route (directional)
	 * - as blockaed
	 * - as selected/highlighted		
	*/
	public void SetDisplayMode(DisplayMode newMode)
	{
		/*switch (newMode)
		{
		case DisplayMode.Normal:
			overlayRenderer.material.mainTexture = null;
			break;

		case DisplayMode.TradeRoute:
			overlayRenderer.material.mainTexture = chevrons;
			break;

		case DisplayMode.Blockaded:
			overlayRenderer.material.mainTexture = blockade;
			break;

		case DisplayMode.Selected:
			
			break;
		}
*/
		currentDisplayMode = newMode;
	}

}

