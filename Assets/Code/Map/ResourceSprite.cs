﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ResourceSprite
{
	public Resource.ResourceType type;
	public Sprite mainSprite;
	public Sprite tributeSprite;

}