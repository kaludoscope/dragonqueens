﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Map :MonoBehaviour {

	private List<PathNode> nodes; 

	public Bounds bounds;
	private List<PathSection> pathSections;

	private Settlement[] allSettlements;

	private int resourcePhaseLatch = 0;
	private int resourcesSent = 0;

	public List<RouteOverlay> routeOverlays;

	public GameObject routeOverlayPrefab;

	public Transform overlayParent;

	public static int totalSettlements;

	public GameObject pawnNodePrefab;

	/*public int totalSettlements
	{
		get {
			return allSettlements.Length;
		}
	}*/

	public void Awake()
	{

		MapDrag drag = Camera.main.GetComponent<MapDrag>();
		//drag.map = this;
		//drag.enabled = true;



		AddPawnNodesToPathSections();

		Init();

		drag.Init(this);
	}

	public void Init()
	{

		Game.instance.messageBroker.SubscribeTo<Route>(Messages.UI.HighlightRoute, UpdateRouteOverlay);
		Game.instance.messageBroker.SubscribeTo(Messages.Events.StartResourcePhase, StartResourcePhase);


		bounds = new Bounds();

		//mapBounds.center = Vector3.zero;

		nodes = new List<PathNode>();
		GetComponentsInChildren<PathNode>(true, nodes);

		allSettlements = GetComponentsInChildren<Settlement>();

		totalSettlements = allSettlements.Length;

		pathSections = new List<PathSection>();
		GetComponentsInChildren<PathSection>(true, pathSections);


		//reset connections
		for (int i = 0; i < nodes.Count; i++)
		{

			bounds.Encapsulate(nodes[i].transform.position);
		}

		//bounds.center += new Vector3(0, 0, 5.0f);

		bounds.Expand(new Vector3(10.0f, 0, 10.0f));

		//bounds.Expand(10.0f);

	}

	public void OnDestroy()
	{
		if (Game.instance != null)
		{
			Game.instance.messageBroker.UnsubscribeFrom<Route>(Messages.UI.HighlightRoute, UpdateRouteOverlay);
			Game.instance.messageBroker.UnsubscribeFrom(Messages.Events.StartResourcePhase, StartResourcePhase);
		}

	}

	public void Start()
	{
		Game.instance.messageBroker.SendMessage(Messages.Events.StartPlay);

	}

	public void UpdateRouteOverlay(Route r)
	{
		RouteOverlay freeOverlay = routeOverlays.Find( x => x.currentRoute == null);

		if (freeOverlay == null)
		{
			var newOverlay =  Instantiate<GameObject>(routeOverlayPrefab, overlayParent);

			freeOverlay = newOverlay.GetComponent<RouteOverlay>();

			routeOverlays.Add(freeOverlay);

		}

		freeOverlay.HighlightRoute(r);
	}

	private void StartResourcePhase()
	{
		resourcesSent = 0;
		resourcePhaseLatch = 0;

		Game.instance.messageBroker.SubscribeTo(Messages.Events.ResourceArrived, OnResourceArrived);

		for (int i = 0; i < allSettlements.Length; i++)
		{
			resourcesSent += allSettlements[i].StartResourcePhase();
		}
	}

	private void OnResourceArrived()
	{
		resourcePhaseLatch ++;

		if (resourcePhaseLatch >= resourcesSent)
		{
			Game.instance.messageBroker.UnsubscribeFrom(Messages.Events.ResourceArrived, OnResourceArrived);

			for (int i = 0; i < allSettlements.Length; i++)
			{
				allSettlements[i].Tick();
			}

			Game.instance.messageBroker.SendMessage(Messages.Events.AllResourcesArrived);

		}

	}


	//editor stuff

	public void AddPawnNodesToPathSections()
	{
		var tempPathSections = new List<PathSection>();
		GetComponentsInChildren<PathSection>(true, tempPathSections);

		foreach (PathSection section in tempPathSections)
		{
			PathNode newNode = Instantiate<GameObject>(pawnNodePrefab, section.transform.position, Quaternion.identity, section.transform).GetComponent<PathNode>();

			section.nodes[0].connectedNodes.Remove(section.nodes[1]);
			section.nodes[1].connectedNodes.Remove(section.nodes[0]);

			section.nodes[0].connectedNodes.Add(newNode);
			section.nodes[1].connectedNodes.Add(newNode);

			newNode.connectedNodes.Add(section.nodes[0]);
			newNode.connectedNodes.Add(section.nodes[1]);
			newNode.connectedSections.Add(section);
		}

	}

}
