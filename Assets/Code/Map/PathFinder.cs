﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PathFindingNode
{
	public int distance;
	public PathFindingNode parent;
	public PathNode node;

	public PathFindingNode(PathFindingNode p, PathNode n, PathNode.Type distanceIncrementer = PathNode.Type.Main)
	{
		
		parent = p;
		node = n;

		if (p == null)
		{
			distance = 0;
		}
		else if (n.type == distanceIncrementer)
		{
			distance = parent.distance + 1;
		}
		else
		{
			distance = parent.distance;
		}
	}
}

public class PathFinder : MonoBehaviour
{
	public PathNode startNode
	{
		get;
		set;
	}

	private int maxDistance = int.MaxValue;

	public Dictionary<PathNode, PathFindingNode> checkedNodes 
	{
		get;
		set;
	}//= new Dictionary<PathNode, PathFindingNode>();

	//private bool isBlockable = false;
	//private bool cacheRoutes = false;
	//private bool castleCanBeDestination = false;

	public void Setup(PathNode start, int dist)
	{
		startNode = start;

	//	isBlockable = blockable;
	//	cacheRoutes = cache;
		maxDistance = dist;

		checkedNodes =  new Dictionary<PathNode, PathFindingNode>();
	}

	/// <summary>
	/// Gets the available PathNode destinations from a starting node for the specifed travel distance. Used for units 
	/// and trade routes.
	/// </summary>
	/// <returns>The available destinations.</returns>
	/// <param name="start">The starting node</param>
	/// <param name="distance">The distance we are able to travel<param>
	/// <param name="canBeBlocked">If set to <c>true</c>, allows for nodes to be blocked (i.e. non-allied settlements)</param>
	public PathNode[] GetAvailableDestinations(bool isBlockable = false, Dragon checkForQueen = null, bool includePawnNodes = false)
	{


	/*	for each node n in Graph:            

			n.distance = INFINITY        

				n.parent = NIL


				create empty queue Q      


				root.distance = 0

				Q.enqueue(root)                      


				while Q is not empty:        



					current = Q.dequeue()



					for each node n that is adjacent to current:

						if n.distance == INFINITY:

							n.distance = current.distance + 1

							n.parent = current

							Q.enqueue(n)

*/


		checkedNodes.Clear();

		PathNode.Type distanceIncrementer = includePawnNodes ? PathNode.Type.Pawn : PathNode.Type.Main;

		if (checkedNodes.Count == 0) 
		{
			checkedNodes.Add(startNode, new PathFindingNode(null, startNode, distanceIncrementer)); //   (new PathFindingNode(start, 0, null));
			Queue<PathNode> pathQueue = new Queue<PathNode>();

			foreach (PathNode node in startNode.connectedNodes)
			{
				if (!isBlockable 
					|| !node.Blocked(checkForQueen))
				{
					pathQueue.Enqueue(node);
					checkedNodes.Add(node, new PathFindingNode(checkedNodes[startNode], node, distanceIncrementer));//(new PathFindingNode(start.connectedNodes[i], 1, start));
				}
			}

			while (pathQueue.Count > 0)
			{
				PathNode current = pathQueue.Dequeue();

				foreach(PathNode node in current.connectedNodes)
				{
					if (!checkedNodes.ContainsKey(node) && checkedNodes[current].distance <= maxDistance)
					{
						if (node.type == distanceIncrementer && checkedNodes[current].distance == maxDistance)
							break;
					
						if (!isBlockable 
							|| !checkedNodes[current].node.Blocked(checkForQueen) 
							|| current == startNode)
						{
							pathQueue.Enqueue(node);
							checkedNodes.Add(node, new PathFindingNode(checkedNodes[current], node, distanceIncrementer));
						}
					}
				}
			}
			checkedNodes.Remove(startNode);
		}
			
		var arr = new PathNode[checkedNodes.Keys.Count];
		//we take off the start node before sending this back
		checkedNodes.Keys.CopyTo(arr, 0);

		return arr;

	}

	public void HighlightAvailableDestinations(bool isBlockable = false, Dragon checkForQueen = null, bool includePawns = false, bool includeCastles = false)
	{
		PathNode [] availableNodes = GetAvailableDestinations(isBlockable, checkForQueen, includePawns);

		for (int i = 0; i < availableNodes.Length; i++)
		{
			//Debug.Log("available node found", availableNodes[i].gameObject);
			availableNodes[i].Highlight(includeCastles, includePawns);
		}

	}

	public LinkedList<PathNode> GetFirstPathTo(PathNode destination, Dragon checkForQueen = null, bool isBlockable = false)
	{
		
			//checkedNodes.Add(startNode, new PathFindingNode(null, startNode)); //   (new PathFindingNode(start, 0, null));

		Queue<PathFindingNode> pathQueue = new Queue<PathFindingNode>();

		PathFindingNode pathFindingStartNode = new PathFindingNode(null, startNode);

		PathFindingNode pathFindingCurrentNode = null;

		foreach (PathNode node in startNode.connectedNodes)
		{
			if (!isBlockable || !node.Blocked(checkForQueen))
			{
				pathQueue.Enqueue(new PathFindingNode(pathFindingStartNode, node));
			}

		}

		while (pathQueue.Count > 0)
		{
			PathFindingNode current = pathQueue.Dequeue();

			foreach(PathNode node in current.node.connectedNodes)
			{
				if (node != current.parent.node)
				{
					if (node == destination)
					{
						pathFindingCurrentNode = new PathFindingNode(current, node);
						pathQueue.Clear();
					}
					else if (!isBlockable || !node.Blocked(checkForQueen))
					{
						pathQueue.Enqueue(new PathFindingNode(current, node));
					}//(new PathFindingNode(start.connectedNodes[i], 1, start));
				}
			}
		}
			

		LinkedList<PathNode> route = new LinkedList<PathNode>();

		route.AddLast(pathFindingCurrentNode.node);

		while (pathFindingCurrentNode.parent != null)
		{
			route.AddLast(pathFindingCurrentNode.parent.node);
			pathFindingCurrentNode = pathFindingCurrentNode.parent;
		}

		return route;
	}
		
	public LinkedList<PathNode> GetFirstPathTo(PathNode destination)
	{

		if (!checkedNodes.ContainsKey(destination))
		{
			GetAvailableDestinations();
		
		}

		return GetPathToDestination(destination);
	}

	/*public void ClearCachedRoutes()
	{
		checkedNodes.Clear();
	}*/

	public void SetNewStartNode(PathNode newNode)
	{
		startNode = newNode;
		checkedNodes.Clear();
	}

	public void SetNewMaxDist(int newDist)
	{
		maxDistance	= newDist;
		checkedNodes.Clear();
	}

	/// <summary>
	/// Gets the path to destination.
	/// </summary>
	/// <returns>The path to the destination.</returns>
	/// <param name="dest">Destination PathNode</param>
	public LinkedList<PathNode> GetPathToDestination(PathNode dest)
	{
		if (checkedNodes.Count > 0)
		{
			if (checkedNodes.ContainsKey(dest))
			{
				PathFindingNode currentNode = checkedNodes[dest];

				LinkedList<PathNode> route = new LinkedList<PathNode>();

				route.AddLast(currentNode.node);

				while (currentNode.parent != null)
				{
					route.AddLast(currentNode.parent.node);
					currentNode = currentNode.parent;
				}

				return route;
			}
		}
		else
		{
			Debug.LogError("Current node list is empty - PathFinder.GetAvailableDestinations must be called first");

		}

		return null;
	}

	public Route GetRouteTo(PathNode dest)
	{
		Route r = new Route(GetPathToDestination(dest));

		return r;
	}

	public void CopyCheckedNodes(PathFinder source)
	{
		checkedNodes = new Dictionary<PathNode, PathFindingNode>(source.checkedNodes);
	}

}

