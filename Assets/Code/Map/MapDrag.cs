﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public class MapDrag : MonoBehaviour {
	public float speed = 0F;

	private Vector3 lastDragPos;

	private Vector3 lastMousePos;

	private Camera mainCam;
	public float orthoZoomSpeed = 0.2f;
	public float perspZoomSpeed = 0.5f;

	public float minZoom = 6.0f;
	public float maxZoom = 20.0f;

	public static bool cameraIsHandlingTouch = false;

	public Vector3 cameraCentre;
	Vector3 cameraOffset = Vector3.zero;
	//private Vector3 lastMousePos;
	//Vector3 lastMousePos;

	private Map map;

	private Bounds cameraBounds;


	private Plane xy;

	void Awake()
	{
		mainCam = GetComponent<Camera>();

		cameraCentre = transform.position;

		Game.instance.messageBroker.SubscribeTo<Dragon>(Messages.Events.SnapToCastle, JumpToCastle);
		Game.instance.messageBroker.SubscribeTo<bool>(Messages.Events.SetDragEnabled, SetEnabled);

		xy = new Plane(Vector3.up, Vector3.zero);
	}

	public void Init(Map m)
	{
		map = m;
		enabled = true;

		cameraBounds = new Bounds();

		xy = new Plane(Vector3.up, Vector3.zero);

		GetCameraBounds();

	}

	private void GetCameraBounds()
	{
		Ray ray;
		float distance;

		ray = mainCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
		xy.Raycast(ray, out distance);

		Vector3 centre = ray.GetPoint(distance);

		ray = mainCam.ViewportPointToRay(new Vector3(0f, 0f, 0f));
		xy.Raycast(ray, out distance);

		Vector3 bottomLeft = ray.GetPoint(distance);

		ray = mainCam.ViewportPointToRay(new Vector3(1f, 0.8f, 0f));
		xy.Raycast(ray, out distance);

		Vector3 topRight = ray.GetPoint(distance);

		float offsetLeft = centre.x - bottomLeft.x;
		float offsetRight = centre.x - topRight.x;
		float offsetTop = centre.z - topRight.z;
		float offsetBottom = centre.z - bottomLeft.z;

		cameraBounds.min= new Vector3( map.bounds.min.x + offsetLeft, 0, map.bounds.min.z + offsetBottom);
		cameraBounds.max = new Vector3(map.bounds.max.x + offsetRight, 0, map.bounds.max.z + offsetTop);

	}

	void LateUpdate() {
		
		Vector3 delta;

		Ray ray;
		float distance;
		Vector3 currentDragPos;

		#if UNITY_EDITOR

		ray = mainCam.ScreenPointToRay(Input.mousePosition);

		xy.Raycast(ray, out distance);

		currentDragPos =ray.GetPoint(distance);

		ray = mainCam.ScreenPointToRay(lastMousePos);

		xy.Raycast(ray, out distance);

		delta = ray.GetPoint(distance) - currentDragPos;


		if (Input.GetMouseButton(0))
		{

			SetNewPos(delta);
		}

		//lastDragPos = ray.GetPoint(distance);
		lastMousePos = Input.mousePosition;

		/*if (Input.GetMouseButtonDown(0))
		{
			drag = true;

			Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			lastDragPos = new Vector2(point.x, point.y);
			lastMousePos = Input.mousePosition;

		}
		else if (Input.GetMouseButton(0) && drag)
		{
			Vector3 point = Camera.main.ScreenToWorldPoint(Input.mousePosition);

			Vector2 dragPos = new Vector2(point.x, point.y);

			Vector2 dragDelta = lastDragPos - dragPos; 

			// Move object across XY plane

			if (Input.mousePosition != lastMousePos)
			{
				//only translate if we've moved the mouse in screen space
				transform.localPosition += dragDelta;
				lastDragPos = dragPos;
			}


			lastMousePos = Input.mousePosition;

		}
		else if (Input.GetMouseButtonUp(0) && drag)
		{
			drag = false;
		}*/

		#endif

		#if UNITY_ANDROID || UNITY_IOS
		if (Input.touchCount == 1) 
		{
			
			if (Input.GetTouch(0).phase == TouchPhase.Moved)
			{

				//cameraIsHandlingTouch = true;

			// Get movement of the finger since last frame
				ray = mainCam.ScreenPointToRay(Input.mousePosition);
				xy.Raycast(ray, out distance);

				currentDragPos = ray.GetPoint(distance);

				ray = mainCam.ScreenPointToRay(lastMousePos);

				xy.Raycast(ray, out distance);



				delta = ray.GetPoint(distance) - currentDragPos;

				SetNewPos(delta);
			}
			/*else if (Input.GetTouch(0).phase == TouchPhase.Ended)
			{
				cameraIsHandlingTouch = false;
			}*/
			// Move object across XY plane
			//transform.Translate(-touchDeltaPosition.x * speed, -touchDeltaPosition.y * Time.deltaTime, 0);
			lastMousePos = Input.GetTouch(0).position;

			//lastDragPos = Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position);
		}

		if (Input.touchCount == 2)
		{
			// Store both touches.
			Touch touchZero = Input.GetTouch(0);
			Touch touchOne = Input.GetTouch(1);

			/*if (touchZero.phase == TouchPhase.Moved)
			{
				cameraIsHandlingTouch = true;
			}
			else if (touchZero.phase == TouchPhase.Ended || touchOne.phase == TouchPhase.Ended)
			{
				cameraIsHandlingTouch = false;
			}*/

			// Find the position in the previous frame of each touch.
			Vector2 touchZeroPrevPos = touchZero.position - touchZero.deltaPosition;
			Vector2 touchOnePrevPos = touchOne.position - touchOne.deltaPosition;

			// Find the magnitude of the vector (the distance) between the touches in each frame.
			float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
			float touchDeltaMag = (touchZero.position - touchOne.position).magnitude;

			// Find the difference in the distances between each frame.
			float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

			// If the camera is orthographic...
			if (mainCam.orthographic)
			{
				// ... change the orthographic size based on the change in distance between the touches.
				mainCam.orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;

				// Make sure the orthographic size never drops below zero.
				//camera.orthographicSize = Mathf.Max(camera.orthographicSize, 6.0f);

				mainCam.orthographicSize = Mathf.Clamp(mainCam.orthographicSize, minZoom, maxZoom);
			}
			else
			{
				Vector3 zoomChange = new Vector3(0, 0, -deltaMagnitudeDiff);

				transform.Translate(zoomChange* perspZoomSpeed, transform);

				cameraCentre += transform.rotation*zoomChange * perspZoomSpeed;



				GetCameraBounds();

				//transform.localPosition += new Vector3 (0, 0, deltaMagnitudeDiff);
			}
		}

		#endif
	}

	void SetNewPos(Vector3 delta, bool isUserInput = true)
	{


		if (isUserInput && (EventSystem.current.IsPointerOverGameObject() || EventSystem.current.IsPointerOverGameObject(0))) return;

		cameraOffset.x += delta.x;
		cameraOffset.z += delta.z;

		// pos clamp
		//cameraOffset.x = Mathf.Clamp(cameraOffset.x, map.bounds.min.x+Camera.main.orthographicSize*Screen.width/Screen.height, map.bounds.max.x - Camera.main.orthographicSize*Screen.width/Screen.height);
		//cameraOffset.y = Mathf.Clamp(cameraOffset.y, map.bounds.min.z+Camera.main.orthographicSize, map.bounds.max.z- Camera.main.orthographicSize);


		//cameraOffset = Vector3.ClampMagnitude(cameraOffset, cameraBounds.size.magnitude);//map.bounds.size.magnitude/2.0f);

		cameraOffset.x = Mathf.Clamp(cameraOffset.x, cameraBounds.min.x, cameraBounds.max.x);
		cameraOffset.z = Mathf.Clamp(cameraOffset.z, cameraBounds.min.z, cameraBounds.max.z);

		/*Vector3 newPos = cameraCentre + cameraOffset;

		newPos.x = Mathf.Clamp(newPos.x, cameraBounds.min.x, cameraBounds.max.x);
		newPos.z = Mathf.Clamp(newPos.z, cameraBounds.min.z, cameraBounds.max.z);*/


//		transform.position = newPos;

		transform.position = cameraCentre + cameraOffset;
	}

	public void JumpToCastle(Dragon queen)
	{
		//Debug.Log("show me dat castle");
		SnapToWorldPos(queen.castle.transform.position);
	}

	public void SnapToWorldPos(Vector3 pos)
	{

		Vector3 delta;

		Ray ray;
		float distance;
		Vector3 currentDragPos;

		ray = mainCam.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0f));
		xy.Raycast(ray, out distance);

		currentDragPos =ray.GetPoint(distance);

		delta = pos - currentDragPos;

		SetNewPos(delta, false);
	}
	
	private void SetEnabled(bool value)
	{
		enabled = value;
	}
}