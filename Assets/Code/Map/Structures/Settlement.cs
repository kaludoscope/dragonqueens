﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PathFinder))]

public class Settlement : Structure {

	public Resource.ResourceType resourceProduced;
	public Resource.ResourceType resourceRequired;

	//prosperity level is beteween 0 - not prosperous (i.e. not allied) and 4 (most prosperous, after being allied for over 20 turns)
	public int prosperityLevel = 0;
	private int turnsAllied = 0;

	[SerializeField]
	private ProsperityData prosperityData;

	private int turnsToResource = 0;
	private int turnsToTribute = 0;
	private int turnsToNeutral = 3;

	private Settlement importSource = null;
	[SerializeField]
	private Settlement exportDest = null;

	//resource phase
	public GameObject resourcePrefab;
	[HideInInspector]
	public bool resourcePhaseFinished = false;
	[HideInInspector]
	public List<Dragon> questVisibleTo = new List<Dragon>();

	private Route exportRoute;
	private Route importRoute;

	public ResourceData resourceData;
	public SpriteRenderer resourceSprite;

	public GameObject banner;

	public SettlementData settlementData;

	private Dictionary<Resource.ResourceType, Resource> heldTribute = new Dictionary<Resource.ResourceType, Resource>();

	public bool ProsperityLocked
	{
		get{
			
			return resourceRequired == Resource.ResourceType.None;
		}
	}


	// Use this for initialization
	protected override void Awake () {

		base.Awake();

		//if (resourceSprite != null)

		Instantiate<GameObject>(settlementData.GetPrefabByResourceType(resourceProduced), transform, false);

			//resourceSprite.sprite = resourceData.GetSpriteByType(resourceProduced);

		pathFinder = GetComponent<PathFinder>();

		pathFinder.Setup(pathNode, 2);


		if (queen != null)
		{
			queen.alliedSettlements.Add(this);
			questVisibleTo.Add(queen);

			prosperityLevel = 1;


			turnsToResource = prosperityData.ExportRates[prosperityLevel].PerTurn;
			turnsToTribute = prosperityData.TributeRates[prosperityLevel].PerTurn;

			RaiseBanner();
		}


	}

	protected override void Start ()
	{
		base.Start ();

		if (exportDest != null)
		{
			pathFinder.GetAvailableDestinations(false);
			InitExportRoute();



		}
	}

	//DATA PROCESSING AND RESOURCE PRODUCTION)

	public void Tick ()
	{


		/*Remaining Tribute is collected
		Trade Goods sent (per settlement)
		Trade Goods received (per settlement)
		Is the Resource Quest satisfied? (per settlement)
		Neutrality Timers activate (per affected settlement)
		Neutrality Timers tick (per affected settlement)
		Settlements neutralise (if their Neutrality Timer reaches its last tick)
		Settlements ally (if their Resource Quest is satisfied and they’re not already allied)
		Land boundaries re-drawn (if applicable)*/

		ProsperityCheck();

		NeutralityCheck();
	}

	private void ProsperityCheck()
	{

		if (importSource != null && importSource.resourceProduced == resourceRequired)
		{
			if (turnsToNeutral < 3) turnsToNeutral = 3;
			
			if (prosperityLevel == 0)
			{
				questVisibleTo.Add(importSource.queen);
				queen = importSource.queen;
				queen.alliedSettlements.Add(this);

				prosperityLevel = 1;

				turnsToResource = prosperityData.ExportRates[prosperityLevel].PerTurn;
				turnsToTribute = prosperityData.TributeRates[prosperityLevel].PerTurn;

				if (parentStructure != null)
					parentStructure.SetDistrictQuestsVisible(queen);

				RaiseBanner();
				//raise the flaaag!
			}
		}
		else if (prosperityLevel > 0 && !ProsperityLocked)
		{
			turnsToNeutral--;
		}
			
	}

	private void RaiseBanner()
	{
		banner.GetComponent<Renderer>().material.mainTexture = queen.data.settlementBanner;  //.sprite = queen.data.settlementBanner;

		banner.SetActive(true);
	}

	private void ClearBanner()
	{
		banner.SetActive(false);
	}

	private void NeutralityCheck()
	{

		/*neutrality timer
		Production of Tribute ceases; timer begins (1 of 3 ‘pie slices’ is removed)
		Timer continues (1 of the remaining 2 ‘pie slices’ is removed)
		Last Trade Goods are sent; timer resolves (last remaining ‘pie slice’ is removed); settlement turns neutral*/

		if (turnsToNeutral == 0)
		{
			prosperityLevel = 0;
			queen.alliedSettlements.Remove(this);
			this.queen = null;
			prosperityLevel = 0;
			turnsToNeutral = 3;
			ClearBanner();
			ClearExportRoute();
		}	
	}

	public void InitExportRoute()
	{
		Debug.Log(exportDest.pathNode, exportDest);

		if (parentStructure == null)
		{
			exportRoute = pathFinder.GetRouteTo(exportDest.pathNode);

		}
		else if (exportDest.parentStructure == this.parentStructure)
		{
			//do the proper thing for town/city districts here
		}

		exportDest.SetImportRoute(exportRoute, this);
	}

	public void SetExportRoute(Settlement newExportDest)
	{
		if (exportDest == newExportDest) return;

		/*if (exportDest != null)
		exportDest.ClearImportRoute();*/

		UnHighlightTradeRoute(exportRoute);

		ClearExportRoute();

		//override the destination's current import route
		if (newExportDest.importSource != null)
		{
			newExportDest.ClearImportRoute();
		}

		exportDest = newExportDest;


		/*if (parentStructure == null)
		{
			exportRoute = pathFinder.GetRouteTo(exportDest.pathNode);
		}
		else if (exportDest.parentStructure == this.parentStructure)
		{
			//do the proper thing for town/city districts here


			exportRoute = GetSubNodeRoute(exportDest);
		}*/

		exportRoute = GetRouteTo(exportDest);

		HighlightTradeRoute(exportRoute);


		exportDest.SetImportRoute(exportRoute, this);

	}

	public Route GetRouteTo(Settlement dest)
	{
		Route r = null;

		if (parentStructure == null || dest.parentStructure != parentStructure)
		{
			r = pathFinder.GetRouteTo(dest.pathNode);
		}
		else if (dest.parentStructure == parentStructure)
		{
			//do the proper thing for town/city districts here

			r = GetSubNodeRoute(dest);
		}

		Debug.Assert(r != null);

		return r;
	}

	public Route GetSubNodeRoute(Settlement dest)
	{
		LinkedList<PathNode> tempNodes = new LinkedList<PathNode>();
		tempNodes.AddFirst(subNode);
		tempNodes.AddFirst(dest.subNode);

		return new Route(tempNodes);
	}

	public void SetImportRoute(Route route, Settlement source)
	{
		importRoute = route;
		importSource = source;

	}

	public void ClearImportRoute()
	{

		if (importSource != null)
			importSource.CancelExport();

		CancelImport();

	}

	public void CancelExport()
	{
		exportDest = null;
		exportRoute = null;
	}
		
	public void ClearExportRoute()
	{
		if (exportDest != null)
			exportDest.CancelImport();

		CancelExport();
	}

	public void CancelImport()
	{
		importSource = null;

		importRoute = null;
	}

	public override void Select (Dragon activeDragon)
	{
		base.Select (activeDragon);

		if (activeDragon == queen)	
		HighlightCurrentTradeRoutes();
	}

	public override void Deselect (Structure s)
	{
		base.Deselect (s);

		if (s != this)
		{
			UnHighlightCurrentTradeRoutes();
		}

	}

	private void HighlightTradeRoute(Route r)
	{
		if (r != null)
		{
			r.currentDisplayMode = Route.DisplayMode.TradeRoute;
			Game.instance.messageBroker.SendMessage<Route>(Messages.UI.HighlightRoute, r);
		}
	}

	private void UnHighlightTradeRoute(Route r)
	{
		if (r != null)
		{
			r.currentDisplayMode = Route.DisplayMode.Normal;
			Game.instance.messageBroker.SendMessage<Route>(Messages.UI.UnhighlightRoute, r);
		}
	}

	public void  HighlightCurrentTradeRoutes()
	{
		HighlightTradeRoute(exportRoute);
		HighlightTradeRoute(importRoute);
	}

	public void UnHighlightCurrentTradeRoutes()
	{
		UnHighlightTradeRoute(exportRoute);
		UnHighlightTradeRoute(importRoute);
	}

	public int StartResourcePhase()
	{
		int numResourcesSent = 0;

		if (prosperityLevel > 0)
		{
			if (exportDest != null)
			{

				if (ProduceExportResources())
					numResourcesSent++;

				/*if (MoveHeldTribute())
					numResourcesSent++;*/

			}
				
			if (ProduceTributeResource())
				numResourcesSent++;
		}

		return numResourcesSent;
	}

	public void ShowAvailableDestinations(Dragon currentPlayerDragon)
	{
		pathFinder.HighlightAvailableDestinations(true, currentPlayerDragon);
	}


	private bool ProduceExportResources()
	{

		//first check for blockades

		//if blockades exist, attempt to reroute 
		//if no reroute available, clear export route

		//then send resources or clear export route accordingly

		turnsToResource --;

		//Debug.Log("produce export resource " + exportDest + exportRoute.Count + turnsToResource);



		if (exportRoute.IsBlockadedFor(queen))
		{
		//	Debug.Log("ROUTE BLOCKADED");

			//it 'sends' but gets destroyed by pawns along the route
			//CreateAndSendExportResource();

			ClearExportRoute();

			turnsToResource = prosperityData.ExportRates[prosperityLevel].PerTurn;
		}
		else if (turnsToResource <= 0)
		{
			CreateAndSendExportResource();

			turnsToResource = prosperityData.ExportRates[prosperityLevel].PerTurn;


			return true;

		}

		return false;


	}

	private void CreateAndSendExportResource()
	{
		GameObject resGO  = Instantiate(resourcePrefab, transform.position, Quaternion.identity) as GameObject;

		Resource res = resGO.GetComponent<Resource>();

		res.type = this.resourceProduced;
		res.SetActiveSprite();

		PathFollower pfollower = res.GetComponent<PathFollower>();

		pfollower.SetRoute(exportRoute);
	}

	private bool MoveHeldTribute()
	{
		if (heldTribute.Count == 0) return false;

		foreach(KeyValuePair<Resource.ResourceType, Resource> pair in heldTribute)
		{
			StartCoroutine(SendTribute(pair.Key, pair.Value.value, 1.5f));
		}

		heldTribute.Clear();

		return true;

	}

	private bool ProduceTributeResource()
	{
		turnsToTribute--;

		if (turnsToTribute == 0 && turnsToNeutral == 3)
		{
			//send some tribute

			StartCoroutine(SendTribute(this.resourceProduced, this.prosperityData.TributeRates[prosperityLevel].ResourcesProduced));

	

			//reset that turn timer
			turnsToTribute = prosperityData.TributeRates[prosperityLevel].PerTurn;

			return true;
		}

		return false;

	}

	private IEnumerator SendTribute(Resource.ResourceType type, int value, float delay = 1.0f)
	{
		yield return new WaitForSeconds(delay);

		LinkedList<PathNode> tributeRoute = pathFinder.GetFirstPathTo(queen.castle.pathNode, queen);

		GameObject resGO  = Instantiate(resourcePrefab, transform.position, transform.rotation) as GameObject;

		Resource res = resGO.GetComponent<Resource>();

		res.isTribute = true;
		res.type = type;
		res.value = value;
		res.SetActiveSprite();

		PathFollower pfollower = res.GetComponent<PathFollower>();



		/*if (tributeRoute.Count > 0)
		{
			int numSettlements = 0;

			for (LinkedListNode<PathNode> node = tributeRoute.First; node != tributeRoute.Last; node = node.Next)
			{
				if (node.Value.type == PathNode.Type.Main)
					numSettlements ++;
			}



			if (numSettlements > 2)
			{
				while (numSettlements > 2)
				{

					if (tributeRoute.First.Value.type == PathNode.Type.Main)
					{
						numSettlements--;
					}

					tributeRoute.RemoveFirst();

				}
					
				LinkedListNode<PathNode> holdingNode = tributeRoute.Last;

				Settlement holdingSettlement = holdingNode.Value.GetComponent<Settlement>();

				res.isHeld = true;

				if (!heldTribute.ContainsKey(res.type))
				{
					holdingSettlement.heldTribute.Add(res.type, res);
				}
				else
				{
					holdingSettlement.heldTribute[res.type].value += res.value;
				}
			}
			//else
			{
				queen.ReceiveTribute(res);
			}


			pfollower.SetRoute(tributeRoute);

		}
		else
		{
			//tribute doesn't get there

			//we send it anyway and it gets 'eaten' by pawns partway
			pfollower.SetRoute(tributeRoute);
		}*/

		pfollower.SetRoute(tributeRoute);
		queen.ReceiveTribute(res);


		/*if (!Map.instance.RouteIsBlockaded(tributeRoute, queen))
		{

			//if (tributeRoute.Count <= 3)
			{
				//res.isHeld = false;
				//tribute is within range and will reach the castle
				//queen gets the tribute
				queen.ReceiveTribute(res);
			}
			else
			{
				while (tributeRoute.Count > 3)
				{
					tributeRoute.RemoveFirst();
				}



				//HACKETY HACK
				/*var dist = 0;

				LinkedListNode<PathNode> holdingNode = tributeRoute.Last;

				while (dist < 2)
				{
					holdingNode = holdingNode.Previous;
					dist ++;
			

				Settlement holdingSettlement = tributeRoute.First.Value.GetComponent<Settlement>();

				res.isHeld = true;

				if (!heldTribute.ContainsKey(res.type))
				{
					holdingSettlement.heldTribute.Add(res.type, res);

				}
				else
				{
					holdingSettlement.heldTribute[res.type].value += res.value;
				}


			}
		}

		pfollower.SetRoute(tributeRoute);
		}*/


		//otherwise it goes partway but gets 'eaten' by pawns when it reaches them

	}

	public override void AddUnit (Unit u)
	{
		base.AddUnit (u);

		if (!questVisibleTo.Contains(u.queen))
			questVisibleTo.Add(u.queen);
	}

	public bool CanSeeQuest(Dragon d)
	{
		return (questVisibleTo.Contains(d) && resourceRequired != Resource.ResourceType.None);
	}

}
