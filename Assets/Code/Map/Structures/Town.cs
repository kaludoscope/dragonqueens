﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Town : Structure {

	protected override void Awake()
	{
		base.Awake();

		GetComponentsInChildren<Settlement>(districts);

		foreach (Settlement district in districts)
		{
			district.parentStructure = this;
		}
	}

	public override void AddUnit(Unit u)
	{
		base.AddUnit(u);

		foreach (Settlement district in districts)
		{
			district.AddUnit(u);
		}
	}

	public void SetDistrictQuestsVisible(Dragon queen)
	{
		foreach (Settlement district in districts)
		{
			district.questVisibleTo.Add(queen);
		}
	}
}
