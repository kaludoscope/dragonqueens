﻿using UnityEngine;
using System.Collections;

public class Castle : Structure {
	

	/*Characteristics of a castle
	This is where tribute goes!
	You can create units from here
	*/

	protected override void Awake()
	{
		base.Awake();

		canBuildUnits = true;

		var spriteRend = GetComponent<SpriteRenderer>();

		spriteRend.sprite = queen.data.castleSprite;

		queen.castle = this;
	}


	protected override void ReceiveImportResource(Resource resource)
	{
		if (resource.isTribute)
		{
		}

	}
}
