﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.EventSystems;

[RequireComponent(typeof(PathNode))]

public class Structure : MonoBehaviour {


	public bool canBuildUnits = false;

	public List<GameObject> availableUnitPrefabs = new List<GameObject>();

	private GameObject highlightSprite;

	protected PathFinder pathFinder;
	public PathNode pathNode {get; private set;}

	public Town parentStructure = null;

	public PathNode subNode;
	//only use this if it's a parent structure!!!!
	public List<Settlement> districts = new List<Settlement>();

	[HideInInspector]
	public Dragon queen = null;

	[SerializeField]
	protected Player.ID initialPlayerID;

	public bool highlighted {

		get {

			if (parentStructure == null)
			{
				return highlightSprite.activeInHierarchy;
			}
			else
			{
				return (parentStructure.highlighted && highlightSprite.activeInHierarchy);
			}

		}
	}

	protected virtual void Awake()
	{

		var h = transform.Find("Highlight");

		if (h) highlightSprite = h.gameObject;

		pathNode = GetComponent<PathNode>() as PathNode;

		Debug.Log(pathNode, this);

		if (pathNode.type == PathNode.Type.Sub)
		{
			subNode = pathNode;
			pathNode = parentStructure.GetComponent<PathNode>();
		}



		if (initialPlayerID != Player.ID.None)
			queen = Game.instance.GetPlayerById(initialPlayerID).dragon;
		

		//Game.instance.messageBroker.SubscribeTo(Messages.Events.ActionCancelled, ClearHighlight);
		//Game.instance.messageBroker.SubscribeTo(Messages.Events.ActionComplete, ClearHighlight);

		Game.instance.messageBroker.SubscribeTo<Structure>(Messages.Events.SettlementSelected, Deselect);



	}

	public virtual void OnDestroy()
	{
		if (Game.instance != null)
		Game.instance.messageBroker.UnsubscribeFrom<Structure>(Messages.Events.SettlementSelected, Deselect);

	}

	protected virtual void OnEnable()
	{
	}

	protected virtual void OnDisable()
	{
	}

	// Use this for initialization
	protected virtual void Start () {
	
	}
	
	// Update is called once per frame
	protected virtual void Update () {
	
	}

	public virtual void Select(Dragon activeDragon)
	{
		ToggleHighlight(true);

		Game.instance.messageBroker.SendMessage<System.Enum, Structure, Dragon>(Messages.UI.OpenPopup, ScreenManager.PopupID.Settlement, this, activeDragon);

		Game.instance.messageBroker.SendMessage<Structure>(Messages.Events.SettlementSelected, this);

		pathNode.available = true;
	}

	public virtual void Deselect(Structure s)
	{
		if (s == this) return;

		ToggleHighlight(false);

		if (pathNode != null)
		pathNode.available = false;

		//Game.instance.messageBroker.UnsubscribeFrom<Structure>(Messages.Events.SettlementSelected, Deselect);
			
	}

	public virtual void OnMouseUpAsButton()
	{

		if (!EventSystem.current.IsPointerOverGameObject() && !EventSystem.current.IsPointerOverGameObject(0))
		{
			/*if ((this is Castle) && PlayStateMachine.currentPlayer != queen)
				return;*/
			
			Game.instance.messageBroker.SendMessage<Structure>(Messages.Input.SettlementClicked, this);
		}

	}


	private void ClearHighlight()
	{
		ToggleHighlight(false);
	}

	public void ToggleHighlight(bool on)
	{

		if (highlightSprite != null) 
		{ 
			if (highlightSprite.activeInHierarchy == on) return;

			highlightSprite.SetActive(on);


		}
	}

	protected virtual void ReceiveImportResource(Resource resource)
	{
		//UPDATE UI HERE
	}		


	public virtual void AddUnit(Unit u)
	{
	}

	public Unit BuildUnit(UnitData.UnitType unitType)
	{
		GameObject prefabToInstantiate = null;

		foreach (GameObject go in availableUnitPrefabs)
		{
			if (go.GetComponent<Unit>().UnitType == unitType)
			{ 
				prefabToInstantiate = go;
				break;
			}

		}

		GameObject newGo = Instantiate(prefabToInstantiate, transform.position, Quaternion.identity, transform.parent) as GameObject;

		Unit newUnit = newGo.GetComponent<Unit>();

		newUnit.Init(this.queen, this);

		return newUnit;

	}

	public virtual int AvailableUnitsOfType(UnitData.UnitType type, Dragon activeDragon)
	{
		int availableUnits = 0;

		foreach (Unit u in pathNode.units)
		{
			if (u.UnitType == type && 
				u.queen == activeDragon && 
				!u.hasMoved)
			{
				availableUnits++;
			}
		}

		return availableUnits;
	}

	public virtual int AllUnitsOfType(UnitData.UnitType type, Dragon activeDragon)
	{
		int availableUnits = 0;

		foreach (Unit u in pathNode.units)
		{
			if (u.UnitType == type && 
				u.queen == activeDragon)
			{
				availableUnits++;
			}
		}

		return availableUnits;
	}

	public virtual int AllUnitsOfType(UnitData.UnitType type)
	{
		int availableUnits = 0;

		foreach (Unit u in pathNode.units)
		{
			if (u.UnitType == type)
			{
				availableUnits++;
			}
		}

		return availableUnits;
	}

	public virtual Unit SelectCurrentUnit(UnitData.UnitType unitType)
	{
		return pathNode.SelectCurrentUnit(unitType);
	}

}
