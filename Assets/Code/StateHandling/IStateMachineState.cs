﻿
public interface IStateMachineState {

	/*SimpleStateMachine stateMachine
	{
		get;
		set;
	}*/

	void OnEnter();
	void OnUpdate();
	void OnExit();

}
