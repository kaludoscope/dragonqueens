﻿using UnityEngine;
using System.Collections;

public class PlayStateMachine : SimpleStateMachine {

	public enum State {

		PlayerAction,
		SettlementInteraction,
		EditTradeRoute,
		UnitInteraction,
		MoveUnit,
		BuildUnit,
		DestroyUnit,
		ResourcePhase,
		TributeCollection,
		TurnEnd
	}
		
	//DEBUG
	[SerializeField]
	private PlayStateMachine.State currentDebugState;

	public override void SetState (System.Enum newState)
	{
		base.SetState (newState);

		currentDebugState = (PlayStateMachine.State)newState;
	}

	//TODO: replace this with something more robust when there's, like, a front end
	public Player [] players = new Player[2];
	public int currentPlayerIndex = 0;

	public Dragon currentPlayerDragon
	{
		get {
			return players[currentPlayerIndex].dragon;
		}
	}

	public override void Awake()
	{


		states.Add(State.PlayerAction, new PlayerActionPlayState(this));
//		states.Add(State.BuildUnit, new BuildUnitPlayState(this));
		states.Add(State.EditTradeRoute, new EditTradeRoutePlayState(this));
//		states.Add(State.MoveUnit, new MoveUnitPlayState(this));
		states.Add(State.ResourcePhase, new ResourcePhasePlayState(this));
		states.Add(State.SettlementInteraction, new SettlementInteractionPlayState(this));
		states.Add(State.UnitInteraction, new UnitInteractionPlayState(this));


		Game.instance.messageBroker.SubscribeTo(Messages.Events.StartPlay, StartTurn);
		//Game.instance.messageBroker.SubscribeTo(Messages.Events.ResetToPlayerAction, ResetPlayerAction);
		Game.instance.messageBroker.SubscribeTo(Messages.Events.ResetToSettlementInteraction, ResetSettlementInteraction);
		Game.instance.messageBroker.SubscribeTo<Player[]>(Messages.Settings.SetPlayers, OnPlayersReceived);
		Game.instance.messageBroker.SubscribeTo(Messages.Events.EndTurn, OnEndTurnReceived);

	}

	public override void Update ()
	{
		base.Update ();

		if (Input.GetKeyDown(KeyCode.Return))
			OpenWinPopup();
		
	}

	private void OnPlayersReceived(Player[] newPlayers)
	{
		players = newPlayers;
		currentPlayerIndex = 0;
	}

	private void StartTurn()
	{
		SetState(State.PlayerAction);

		currentPlayerDragon.ActivatePlayer();
		currentPlayerDragon.BeginTurn();

		Game.instance.messageBroker.SendMessage<Dragon>(Messages.UI.UpdatePlayerDisplay, currentPlayerDragon);
		Game.instance.messageBroker.SendMessage<Dragon>(Messages.Events.SnapToCastle, currentPlayerDragon);
		Game.instance.messageBroker.SendMessage<Player, Player>(Messages.UI.UpdateRealmMeter, players[0], players[1]);


	}

	private void ResetPlayerAction()
	{
		SetState(State.PlayerAction);

	}
		
	private void ResetSettlementInteraction()
	{
		SetState(State.SettlementInteraction);
	}

	private void OnEndTurnReceived()
	{
		SetState(State.ResourcePhase);
	}
		

	public void NextPlayer()
	{
		if (!WinCheck())
		{

			currentPlayerDragon.DeactivatePlayer();

			currentPlayerIndex = (currentPlayerIndex + 1) % players.Length;

			Game.instance.messageBroker.SendMessage(Messages.Events.StartPlay);

		}
		else
		{
			OpenWinPopup();
		}
	}

	private bool WinCheck()
	{
		return (currentPlayerDragon.alliedSettlements.Count > Map.totalSettlements/2);

		/*for (int i = 0; i < players.Length; i++)
			{
				players[i].dragon.Reset();
			}*/
	}

	private void OpenWinPopup()
	{

		var popupData = new ConfirmationPopupData("A winner is you", string.Format("You won with {0} out of {1} settlements", currentPlayerDragon.alliedSettlements.Count, Map.totalSettlements), Messages.Events.GameOver);

		Game.instance.messageBroker.SendMessage<System.Enum, ConfirmationPopupData>(Messages.UI.OpenPopup, ScreenManager.PopupID.Information, popupData); 
	}
}
