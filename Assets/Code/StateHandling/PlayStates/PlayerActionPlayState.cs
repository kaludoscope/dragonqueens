﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;


public class PlayerActionPlayState: PlayStateMachineState
{
	public PlayerActionPlayState (PlayStateMachine _stateMachine) : base (_stateMachine)
	{
	}
	public override void OnEnter()
	{
		Game.instance.messageBroker.SubscribeTo<Structure>(Messages.Input.SettlementClicked, OnReceiveStructureClick);
		Game.instance.messageBroker.SubscribeTo<Unit>(Messages.Input.UnitClicked, OnReceiveUnitClick);
		//this should reset us to 'nothing is selected'

		Game.instance.messageBroker.SendMessage<Structure>(Messages.Events.SettlementSelected, null);

		Game.instance.messageBroker.SendMessage(Messages.Events.ClearAllHighlights);

		stateMachine.lastSelectedObject = null;
	}

	public override void OnExit()
	{
		Game.instance.messageBroker.UnsubscribeFrom<Structure>(Messages.Input.SettlementClicked, OnReceiveStructureClick);
		Game.instance.messageBroker.UnsubscribeFrom<Unit>(Messages.Input.UnitClicked, OnReceiveUnitClick);
	}
		
	public void OnReceiveStructureClick(Structure s)
	{

		stateMachine.lastSelectedObject = s;

		stateMachine.SetState(PlayStateMachine.State.SettlementInteraction);
	}

	public void OnReceiveUnitClick(Unit u)
	{
		stateMachine.lastSelectedObject = u;

		if (!u.hasMoved && u.queen == stateMachine.currentPlayerDragon)
		stateMachine.SetState(PlayStateMachine.State.UnitInteraction);
	}

	///DEBUG

	public override void OnUpdate ()
	{
		base.OnUpdate ();

		if (Input.GetKeyDown("space"))
			Game.instance.messageBroker.SendMessage(Messages.Events.EndTurn);

	}
}