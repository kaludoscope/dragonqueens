﻿
using UnityEngine;
using System.Collections;

public class UnitInteractionPlayState : PlayStateMachineState
{
	private Unit selectedUnit;

	public UnitInteractionPlayState (PlayStateMachine _stateMachine) : base (_stateMachine)
	{
	}

	private Route selectedRoute;
	private Structure lastSelectedStructure;

	private PathNode destinationNode;

	public override void OnEnter ()
	{
		base.OnEnter ();

		Debug.Assert(stateMachine.lastSelectedObject is Unit);

		selectedUnit = stateMachine.lastSelectedObject as Unit;

//		Game.instance.messageBroker.SubscribeTo(Messages.Input.MoveUnitClicked, OnMoveUnitReceived);
		Game.instance.messageBroker.SubscribeTo(Messages.Input.DestroyUnitClicked, OnDestroyUnitReceived);
		Game.instance.messageBroker.SubscribeTo(Messages.Input.ActionCancelClicked, OnReceiveCancelAction);

		Game.instance.messageBroker.SubscribeTo<PathNode>(Messages.Input.PathNodeClicked, OnReceivePathNodeClick);
		Game.instance.messageBroker.SubscribeTo<int>(Messages.UI.ConfirmDestination, OnReceiveConfirmDestination);
		Game.instance.messageBroker.SubscribeTo(Messages.UI.ConfirmDestination, OnReceiveConfirmDestination);
		Game.instance.messageBroker.SubscribeTo(Messages.UI.CancelDestination, OnReceiveCancelDestination);

		lastSelectedStructure = selectedUnit.currentStructure;

		//get available destinations from selected unit pathfinder & highlight them
		selectedUnit.ShowAvailableDestinations();

		//We shouldn't be able to interact with the UI at this stage
		Game.instance.messageBroker.SendMessage(Messages.UI.DisablePopup);
	}

	public override void OnExit ()
	{
		base.OnExit ();

		selectedUnit = null;
		lastSelectedStructure = null;
		selectedRoute = null;
		destinationNode = null;

		//Bring that UI back

		//Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.MoveUnitClicked, OnMoveUnitReceived);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.DestroyUnitClicked, OnDestroyUnitReceived);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.ActionCancelClicked, OnReceiveCancelAction);
		Game.instance.messageBroker.UnsubscribeFrom<PathNode>(Messages.Input.PathNodeClicked, OnReceivePathNodeClick);
		Game.instance.messageBroker.UnsubscribeFrom<int>(Messages.UI.ConfirmDestination, OnReceiveConfirmDestination);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.ConfirmDestination, OnReceiveConfirmDestination);

		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.CancelDestination, OnReceiveCancelDestination);

		Game.instance.messageBroker.SendMessage(Messages.UI.EnablePopup);


	}

	private void OnReceivePathNodeClick(PathNode node)
	{
		PathFinder pathfinder = stateMachine.lastSelectedObject.GetComponent<PathFinder>();

		//check for broken shit
		Debug.Assert(pathfinder != null);

		if (node.connectedStructure != null)
			node = node.connectedStructure.pathNode;

		if (!node.available || node == pathfinder.startNode || node == destinationNode) return;

		if (selectedRoute != null)
		{
			//clear any previous selected routes

			selectedRoute.SetDisplayMode(Route.DisplayMode.Normal);
			Game.instance.messageBroker.SendMessage<Route>(Messages.UI.UnhighlightRoute, selectedRoute);
		}

		destinationNode = node;

		selectedRoute = pathfinder.GetRouteTo(node);

		selectedRoute.SetDisplayMode(Route.DisplayMode.Selected);

		Game.instance.messageBroker.SendMessage<Route>(Messages.UI.HighlightRoute, selectedRoute);
		//Game.instance.messageBroker.SendMessage<System.Enum>

		var popupData = new ConfirmationPopupData("Send unit here?", "", Messages.UI.ConfirmDestination, Messages.UI.CancelDestination, destinationNode.transform.position);


		if (selectedUnit.HasSiblings())
		{
			Game.instance.messageBroker.SendMessage<System.Enum, Unit, ConfirmationPopupData>(Messages.UI.OpenPopup, ScreenManager.PopupID.MultiUnits, selectedUnit, popupData); 

		}
		else
		{
			Game.instance.messageBroker.SendMessage<System.Enum, ConfirmationPopupData>(Messages.UI.OpenPopup, ScreenManager.PopupID.SmallConfirm, popupData); 
		}
	}

	private void OnReceiveConfirmDestination()
	{
		OnReceiveConfirmDestination(1);
	}

	private void OnReceiveConfirmDestination(int numUnits)
	{
		//SEND THE UNIT THERE

		selectedRoute.SetDisplayMode(Route.DisplayMode.Normal);
		Game.instance.messageBroker.SendMessage<Route>(Messages.UI.UnhighlightRoute, selectedRoute);

		if (numUnits == 1)
		{
			selectedUnit.GoTo(destinationNode);
		}
		else
		{
			Unit [] units = selectedUnit.GetSiblings(numUnits);

			for (int i = 0; i < units.Length; i++)
			{
				units[i].GoTo(destinationNode);
			}
		}

		stateMachine.lastSelectedObject = lastSelectedStructure;

		NextState();

	}

	private void OnReceiveCancelDestination()
	{
		//destination choice is cancelled but we are still in unit move state
		selectedRoute.SetDisplayMode(Route.DisplayMode.Normal);
		Game.instance.messageBroker.SendMessage<Route>(Messages.UI.UnhighlightRoute, selectedRoute);


		selectedRoute = null;
	}

	/*public void OnMoveUnitReceived()
	{
		stateMachine.SetState(PlayStateMachine.State.MoveUnit);
	}*/

	public void OnDestroyUnitReceived()
	{
		Game.instance.messageBroker.SubscribeTo(Messages.UI.Confirm, OnConfirmDestroyUnit);

		var confirmationPopupData = new ConfirmationPopupData("Destroy this unit?", string.Format("You'll regain {0} magic", Mathf.Abs(selectedUnit.unitData.destructionCost)), Messages.UI.Confirm, Messages.UI.Cancel);

		//open confirmation popup
		Game.instance.messageBroker.SendMessage<System.Enum, ConfirmationPopupData>(Messages.UI.OpenPopup, ScreenManager.PopupID.Confirm, confirmationPopupData);

	}

	private void OnConfirmDestroyUnit()
	{
		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.Confirm, OnConfirmDestroyUnit);

		stateMachine.lastSelectedObject = selectedUnit.currentStructure;

		selectedUnit.DestroySelf();

		NextState();

	}

	private void NextState()
	{
		if (stateMachine.lastSelectedObject != null)
		{
			stateMachine.SetState(PlayStateMachine.State.SettlementInteraction);
		}
		else
		{
			stateMachine.SetState(PlayStateMachine.State.PlayerAction);

		}
	}

	private void OnReceiveCancelAction()
	{
		stateMachine.lastSelectedObject = selectedUnit.currentStructure;

		NextState();
	}
}

