﻿using UnityEngine;
using System.Collections;

public class ResourcePhasePlayState : PlayStateMachineState
{

	public ResourcePhasePlayState (PlayStateMachine _stateMachine) : base (_stateMachine)
	{
	}

	public override void OnEnter ()
	{
		base.OnEnter ();

		Game.instance.messageBroker.SendMessage(Messages.Events.StartResourcePhase);

		Game.instance.messageBroker.SubscribeTo(Messages.Events.AllResourcesArrived, PhaseComplete);

		Game.instance.messageBroker.SendMessage(Messages.Events.ClearAllHighlights);

	}

	public override void OnExit ()
	{
		base.OnExit ();

		Game.instance.messageBroker.UnsubscribeFrom(Messages.Events.AllResourcesArrived, PhaseComplete);
	}

	private void PhaseComplete()
	{
		stateMachine.NextPlayer();
	}

	//DEBUG

	public override void OnUpdate ()
	{
		base.OnUpdate ();
		if (Input.GetKeyDown("space"))
		{
			PhaseComplete();
		}
	}
}
