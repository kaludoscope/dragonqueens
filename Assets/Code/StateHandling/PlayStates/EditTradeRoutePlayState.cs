﻿using UnityEngine;
using System.Collections;
using System;

public class EditTradeRoutePlayState : PlayStateMachineState
{

	private Route selectedRoute;

	private Settlement selectedSettlement;

	private Settlement destinationSettlement;

	public EditTradeRoutePlayState (PlayStateMachine _stateMachine) : base (_stateMachine)
	{
	}
		
	public override void OnEnter ()
	{
		Game.instance.messageBroker.SubscribeTo<Structure>(Messages.Input.SettlementClicked, OnReceiveStructureClicked);
		Game.instance.messageBroker.SubscribeTo(Messages.UI.CancelDestination, OnReceiveCancelTradeRoute);
		Game.instance.messageBroker.SubscribeTo(Messages.UI.ConfirmDestination, OnReceiveConfirmTradeRoute);
		Game.instance.messageBroker.SubscribeTo(Messages.Input.ActionCancelClicked, OnReceiveCancelAction);

		Debug.Assert(stateMachine.lastSelectedObject is Settlement);

		selectedSettlement = stateMachine.lastSelectedObject as Settlement;

		selectedSettlement.ShowAvailableDestinations(stateMachine.currentPlayerDragon);

	}

	public override void OnExit ()
	{

		Game.instance.messageBroker.UnsubscribeFrom<Structure>(Messages.Input.SettlementClicked, OnReceiveStructureClicked);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.ActionCancelClicked, OnReceiveCancelAction);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.CancelDestination, OnReceiveCancelTradeRoute);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.ConfirmDestination, OnReceiveConfirmTradeRoute);

		selectedRoute = null;
		//selectedSettlement = null;
		destinationSettlement = null;
	}

	private void OnReceiveStructureClicked(Structure s)
	{
		//fail silently if we've clicked on a structure that isn't available to us
		if (!s.pathNode.available) return;

		//if you click the same one we do a big old ignore
		if (s == selectedSettlement || s == destinationSettlement) return;

		Game.instance.messageBroker.SendMessage<Route>(Messages.UI.UnhighlightRoute, selectedRoute);

		destinationSettlement = s as Settlement;

		//PathFinder pathfinder = stateMachine.lastSelectedObject.GetComponent<PathFinder>();

		//pathfinder shouldn't be null
		//Debug.Assert(pathfinder != null);

		selectedRoute = selectedSettlement.GetRouteTo(destinationSettlement);;

		//selectedRoute = pathfinder.GetRouteTo(s.pathNode);

		selectedRoute.SetDisplayMode(Route.DisplayMode.Selected);

		Game.instance.messageBroker.SendMessage<Route>(Messages.UI.HighlightRoute, selectedRoute);

		var popupData = new ConfirmationPopupData("Send resources here?", "", Messages.UI.ConfirmDestination, Messages.UI.CancelDestination, destinationSettlement.transform.position);

		Game.instance.messageBroker.SendMessage<Enum, ConfirmationPopupData>(Messages.UI.OpenPopup, ScreenManager.PopupID.SmallConfirm, popupData); 


	}

	private void OnReceiveConfirmTradeRoute()
	{
		selectedRoute.SetDisplayMode(Route.DisplayMode.Normal);
		Game.instance.messageBroker.SendMessage<Route>(Messages.UI.UnhighlightRoute, selectedRoute);

		selectedSettlement.SetExportRoute(destinationSettlement);

		stateMachine.SetState(PlayStateMachine.State.SettlementInteraction);

	}

	private void OnReceiveCancelTradeRoute()
	{
		selectedRoute.SetDisplayMode(Route.DisplayMode.Normal);
		Game.instance.messageBroker.SendMessage<Route>(Messages.UI.UnhighlightRoute, selectedRoute);


		selectedRoute = null;
		destinationSettlement = null;
	}

	private void OnReceiveCancelAction()
	{
		stateMachine.SetState(PlayStateMachine.State.SettlementInteraction);
	}
}

