﻿using System;
using UnityEngine;

public class SettlementInteractionPlayState: PlayStateMachineState
{

	public SettlementInteractionPlayState (PlayStateMachine _stateMachine) : base (_stateMachine)
	{
	}

	public override void OnEnter ()
	{
		Debug.Assert(stateMachine.lastSelectedObject is Structure);

		(stateMachine.lastSelectedObject as Structure).Select(stateMachine.currentPlayerDragon);

		Game.instance.messageBroker.SubscribeTo(Messages.Input.SetTradeRouteClicked, OnReceiveResourceSelected);
		Game.instance.messageBroker.SubscribeTo<Unit>(Messages.Input.UnitClicked, OnReceiveUnitSelected);
		Game.instance.messageBroker.SubscribeTo(Messages.Input.BuildUnitClicked, OnReceiveBuildUnit);
		Game.instance.messageBroker.SubscribeTo<Structure>(Messages.Input.SettlementClicked, OnReceiveStructureClick);
		Game.instance.messageBroker.SubscribeTo(Messages.Input.ActionCancelClicked, OnReceiveActionCancel);

	}

	public override void OnExit ()
	{
		Game.instance.messageBroker.SendMessage<Enum>(Messages.UI.ClosePopup, ScreenManager.PopupID.Settlement);

		Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.SetTradeRouteClicked, OnReceiveResourceSelected);
		Game.instance.messageBroker.UnsubscribeFrom<Unit>(Messages.Input.UnitClicked, OnReceiveUnitSelected);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.BuildUnitClicked, OnReceiveBuildUnit);
		Game.instance.messageBroker.UnsubscribeFrom<Structure>(Messages.Input.SettlementClicked, OnReceiveStructureClick);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.ActionCancelClicked, OnReceiveActionCancel);

	}

	public void OnReceiveStructureClick(Structure s)
	{
		s.Select(stateMachine.currentPlayerDragon);

		stateMachine.lastSelectedObject = s;
	}

	private void OnReceiveResourceSelected()
	{
		stateMachine.SetState(PlayStateMachine.State.EditTradeRoute);
	}

	private void OnReceiveUnitSelected(Unit u)
	{
		stateMachine.lastSelectedObject = u;

		if (!u.hasMoved && u.queen == stateMachine.currentPlayerDragon)
		stateMachine.SetState(PlayStateMachine.State.UnitInteraction);
	}

	private void OnReceiveBuildUnit()
	{
		stateMachine.SetState(PlayStateMachine.State.BuildUnit);
	}

	private void OnReceiveActionCancel()
	{
		stateMachine.SetState(PlayStateMachine.State.PlayerAction);
	}


	///DEBUG

	public override void OnUpdate ()
	{
		base.OnUpdate ();

		if (Input.GetKeyDown("space"))
		Game.instance.messageBroker.SendMessage(Messages.Events.EndTurn);

	}
}
