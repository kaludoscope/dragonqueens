﻿using UnityEngine;
using System.Collections;

public class PlayStateMachineState : IStateMachineState
{
	public PlayStateMachine stateMachine
	{
		get;
		set;
	}



	public PlayStateMachineState (PlayStateMachine _stateMachine)
	{
		stateMachine = _stateMachine;
	}

	public virtual void OnEnter ()
	{
	}

	public virtual void OnUpdate ()
	{
	}

	public virtual void OnExit ()
	{
	}

}

