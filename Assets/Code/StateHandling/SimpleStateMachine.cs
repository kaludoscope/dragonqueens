﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public abstract class SimpleStateMachine : MonoBehaviour {

	protected Dictionary<Enum, IStateMachineState> states = new Dictionary<Enum, IStateMachineState>();

	protected IStateMachineState currentState;
	protected IStateMachineState lastState;

	public MonoBehaviour lastSelectedObject;

	public Enum currentStateKey;

	public virtual void Awake ()
	{
	}

	// Use this for initialization
	public virtual void Start () 
	{
	
	}
	
	// Update is called once per frame
	public virtual void Update () 
	{
		if (currentState != null)
		currentState.OnUpdate();

	}

	public virtual void SetState(Enum newState)
	{


		Debug.Assert(states.ContainsKey(newState));

			lastState = currentState;
		currentState = states[newState];

		currentStateKey = newState;



		if (lastState != null)
		lastState.OnExit();

		//NOTE: may need to wait a frame here

		currentState.OnEnter();

	}
}
