﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameStateMachine : SimpleStateMachine
{
	[SerializeField]
	private PlayStateMachine playStateMachine;



	[SerializeField]
	private GameObject map;

	public enum State {

		None,
		Menu,
		CharacterSelect,
		InGame,
		GameOver

	}

	// Use this for initialization
	public override void Awake()
	{

		states.Add(State.Menu, new MenuGameState(this));
		states.Add(State.CharacterSelect, new CharacterSelectGameState(this));
		states.Add(State.InGame, new PlayGameState(this));
		states.Add(State.GameOver, new GameOverGameState(this));

	}

	public override void Start()
	{
		SetState(State.Menu);

		Game.instance.messageBroker.SubscribeTo(Messages.Events.LoadLevel, GameStart);

	}

	protected void GameStart()
	{
		//Instantiate(Game.instance.Levels[0], Vector3.zero, Quaternion.identity, Game.instance.transform);

		SceneManager.LoadScene("Level0", LoadSceneMode.Additive);

	}

}

