﻿using UnityEngine;
using System.Collections;

public class GameStateMachineState : IStateMachineState
{
	public SimpleStateMachine stateMachine
	{
		get;
		set;
	}



	public GameStateMachineState (SimpleStateMachine _stateMachine)
	{
		stateMachine = _stateMachine;
	}

	public virtual void OnEnter ()
	{
	}

	public virtual void OnUpdate ()
	{
	}

	public virtual void OnExit ()
	{
	}

}

