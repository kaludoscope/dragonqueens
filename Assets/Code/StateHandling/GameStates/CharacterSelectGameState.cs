﻿using UnityEngine;
using System.Collections;

public class CharacterSelectGameState:GameStateMachineState
{
	public CharacterSelectGameState (SimpleStateMachine _stateMachine) :base(_stateMachine)
	{
	}

	public override void OnEnter ()
	{
		Game.instance.messageBroker.SubscribeTo<System.Enum>(Messages.UI.Confirm, OnCharacterConfirm);

		Game.instance.messageBroker.SendMessage<System.Enum>(Messages.UI.OpenScreen, ScreenManager.ScreenID.CharacterSelect);

	}

	public override void OnExit ()
	{
		Game.instance.messageBroker.UnsubscribeFrom<System.Enum>(Messages.UI.Confirm, OnCharacterConfirm);
	}

	public void OnCharacterConfirm(System.Enum message)
	{
		if (message.Equals(ScreenManager.ScreenID.CharacterSelect))
			stateMachine.SetState(GameStateMachine.State.InGame);
	}

}

