﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class PlayGameState:GameStateMachineState
{
	public PlayGameState (SimpleStateMachine _stateMachine) :base(_stateMachine)
	{
	}

	public override void OnEnter ()
	{
		Game.instance.messageBroker.SendMessage<System.Enum>(Messages.UI.OpenScreen, ScreenManager.ScreenID.Play);
		Game.instance.messageBroker.SendMessage(Messages.Events.LoadLevel);
		Game.instance.messageBroker.SubscribeTo(Messages.Events.GameOver, OnGameOver);
	}

	private void OnGameOver()
	{
		stateMachine.SetState(GameStateMachine.State.Menu);

		SceneManager.UnloadSceneAsync("Level0");

	}

	public override void OnExit ()
	{
		Game.instance.messageBroker.UnsubscribeFrom(Messages.Events.GameOver, OnGameOver);

	}

}

