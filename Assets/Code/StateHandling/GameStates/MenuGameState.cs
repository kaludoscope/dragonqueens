﻿using UnityEngine;
using System.Collections;

public class MenuGameState:GameStateMachineState
{
	public MenuGameState (SimpleStateMachine _stateMachine) :base(_stateMachine)
	{
	}

	public override void OnEnter ()
	{
		Game.instance.messageBroker.SendMessage<System.Enum>(Messages.UI.OpenScreen, ScreenManager.ScreenID.Menu);
	}

	public override void OnUpdate ()
	{
		if (Input.GetMouseButton(0) || Input.touches.Length > 0)
		{
			stateMachine.SetState(GameStateMachine.State.CharacterSelect);
		}	
	}

	public override void OnExit ()
	{
	}

}

