﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceData : ScriptableObject {

	public List<ResourceSprite> sprites = new List<ResourceSprite>();

	public Dictionary<Resource.ResourceType, ResourceSprite> spriteLookup = new Dictionary<Resource.ResourceType, ResourceSprite>();

	public Sprite GetSpriteByType(Resource.ResourceType t, bool isTribute = false)
	{
		if (spriteLookup.Count == 0)
		{
			foreach (ResourceSprite sprite in sprites)
			{
				spriteLookup.Add(sprite.type, sprite);
			}
		}

		return (isTribute ? spriteLookup[t].tributeSprite : spriteLookup[t].mainSprite);
	}

}

