﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class GameData : MonoBehaviour
{

	//NOTE: prosperity levels are 0 (neutral) to 4 (very prosperous/long alliance)
	public int[] turnsToReachProsperityLevel;

	public enum StructureType
	{
		Castle,
		Village,
		TownDistrict,
		CityDistrict

	}

	public ProsperityData [] prosperityData;

	public UnitData [] unitData;

	public Dictionary<GameData.StructureType, ProsperityData> prosperityDataLookup;
	public Dictionary<UnitData.UnitType, UnitData> unitDataLookup;

	void Awake()
	{
		
		//make a lookup dictionary for dat Prosperity data here


		prosperityDataLookup = new Dictionary<StructureType, ProsperityData>();

		for (int i = 0; i < prosperityData.Length; i++)
		{
			prosperityDataLookup.Add(prosperityData[i].type, prosperityData[i]);
		}

		unitDataLookup = new Dictionary<UnitData.UnitType, UnitData>();

		for (int i = 0; i < unitData.Length; i++)
		{
			unitDataLookup.Add(unitData[i].type, unitData[i]);
		}


	}

	// Use this for initialization
	void Start ()
	{
	
	}
	
	// Update is called once per frame
	void Update ()
	{
	
	}
}

