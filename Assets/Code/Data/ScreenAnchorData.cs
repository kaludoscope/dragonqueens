﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ScreenAnchorData : ScriptableObject
{
	[System.Serializable]
	public class AnchorPos
	{
		public Vector2 pos;
		public AnchorPresets presetName;
	}

	public AnchorPos[] screenAnchors;
}

