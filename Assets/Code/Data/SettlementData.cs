﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class SettlementPrefab
{
	public Resource.ResourceType type;
	public GameObject prefab;
}

public class SettlementData : ScriptableObject
{
	public SettlementPrefab[] prefabs;

	public GameObject GetPrefabByResourceType(Resource.ResourceType t)
	{
		for (int i = 0; i < prefabs.Length; i++)
		{
			if (prefabs[i].type == t)
				return prefabs[i].prefab;
		}

		return null;
	}
}

