﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ProsperityData:ScriptableObject {

	public GameData.StructureType type;

	public ResourceProductionRate [] ExportRates;
	public ResourceProductionRate [] TributeRates;

}
