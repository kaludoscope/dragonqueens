﻿using UnityEngine;
using System.Collections;

//[CreateAssetMenu(menuName = "DragonData")]
[System.Serializable]
public class DragonData : ScriptableObject {

	public string DragonName;

	public Game.DragonId id;


	public Sprite unitBanner;
	public Sprite mainBanner;
	public Texture settlementBanner;

	public Sprite dragonIcon;
	public Sprite dragonPortrait;

	public Resource.ResourceType favouredTribute;

	public Sprite castleSprite;

}
