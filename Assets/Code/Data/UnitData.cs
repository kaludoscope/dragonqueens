﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class UnitData : ScriptableObject {

	public int creationCost;
	public int maintenanceCost;
	public int transformationCost;
	public int destructionCost;

	public int moveDistance;

	public UnitType type;

	public enum UnitType
	{
		Pawn,
		Spectre,
		Envoy
	}

	public static Dictionary<string, UnitType> UnitTypesByString = new Dictionary<string, UnitType>()
	{
		{ "Pawn", UnitType.Pawn},
		{ "Spectre", UnitType.Spectre},
		{ "Envoy", UnitType.Envoy}
	};
}
