﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class ResourceProductionRate
{
	public int ResourcesProduced;
	public int PerTurn;
}

