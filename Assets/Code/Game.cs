﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

public class Game : MonoSingleton<Game>
{	
	
	public MessageBroker messageBroker;

	public List<Dragon> dragons;

	private Dictionary<DragonId, Dragon> dragonsById = new Dictionary<DragonId, Dragon>();

	public Player[] players;

	public ScreenAnchorData screenAnchorData;

	public enum DragonId
	{
		Maude,
		Evelyn
	}
		
	public List<GameObject> Levels;

	void Awake()
	{
		Screen.orientation = ScreenOrientation.AutoRotation;

		Screen.autorotateToLandscapeLeft = true;

		Screen.autorotateToLandscapeRight = true;
		Screen.autorotateToPortrait = false;
		Screen.autorotateToPortraitUpsideDown = false;

		Vector2 screenSize = new Vector2(Screen.width, Screen.height);

		Debug.Log(screenSize);

		for (int i = 0; i < screenAnchorData.screenAnchors.Length; i++)
		{

			screenAnchorData.screenAnchors[i].pos = Vector2.Scale(screenAnchorData.screenAnchors[i].pos, screenSize);
			//screenAnchorData.screenAnchors[i].pos *= screenSize;
		}

		messageBroker = new MessageBroker();

		players = new Player[2];

		foreach (Dragon d in dragons)
		{
			dragonsById.Add(d.data.id, d);
		}

		messageBroker.SubscribeTo<DragonId[]>(Messages.Settings.SetPlayers, InitPlayers);
	}

	public void InitPlayers(DragonId[] playerDragons)
	{
		for (int i = 0; i < players.Length; i++)
		{
			if (players[i] == null)
			{
				players[i] = new Player(dragonsById[playerDragons[i]], (Player.ID)(i+1));
			}
			else
			{
				
				players[i].dragon = dragonsById[playerDragons[i]];
				players[i].dragon.Reset();
			}

		}

		messageBroker.SendMessage<Player[]>(Messages.Settings.SetPlayers, players);
	}

	public Player GetPlayerById(Player.ID id)
	{
		return players[(int)id - 1];
	}

	public void OnApplicationQuit()
	{
		if (SceneManager.sceneCount > 1)
			SceneManager.UnloadSceneAsync("Level0");
		
		Resources.UnloadAsset(screenAnchorData);
	}

}

