﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class ScreenManager : MonoBehaviour
{

	public List<UIScreen> Screens = new List<UIScreen>();
	public List<UIPopupBase> Popups = new List<UIPopupBase>();

	private Dictionary< GameStateMachine.State, UIScreen> stateScreens = new Dictionary<GameStateMachine.State, UIScreen>();

	private Dictionary<Enum, UIPopupBase> popupsByID = new Dictionary<Enum, UIPopupBase>();
	private Dictionary<Enum, UIScreen> screensByID = new Dictionary<Enum, UIScreen>();

	private UIScreen activeScreen = null;
	private Stack<UIPopupBase> activePopups = new Stack<UIPopupBase>();
	private UIPopupBase topPopup;


	public enum PopupID
	{
		None,
		Settlement,
		Confirm,
		SmallConfirm,
		UnitInteract,
		BuildUnit,
		Information,
		MultiUnits

	}

	public enum ScreenID
	{
		None,
		Menu,
		CharacterSelect,
		Play,
		GameOver
	}

	// Use this for initialization
	void Awake ()
	{
	
		foreach (UIScreen s in Screens)
		{
			if (s.LinkedState != GameStateMachine.State.None)
			{
				stateScreens.Add(s.LinkedState, s);
			}
		}


		foreach (UIPopupBase popup in Popups)
		{
			if (popup.id != PopupID.None && !popupsByID.ContainsKey(popup.id))
			{
				popupsByID.Add(popup.id, popup);
			}
		}

		foreach (UIScreen screen in Screens)
		{
			if (screen.id != ScreenID.None && !screensByID.ContainsKey(screen.id))
			{
				screensByID.Add(screen.id, screen);
			}
		}

		Game.instance.messageBroker.SubscribeTo<Enum>(Messages.UI.OpenScreen, OpenScreen);

		//Popup open subscriptions
		Game.instance.messageBroker.SubscribeTo<Enum, Structure, Dragon>(Messages.UI.OpenPopup, OpenPopupOnMessage<Structure, Dragon>);
		Game.instance.messageBroker.SubscribeTo<Enum, ConfirmationPopupData>(Messages.UI.OpenPopup, OpenPopupOnMessage<ConfirmationPopupData>);
		Game.instance.messageBroker.SubscribeTo<Enum, Dragon>(Messages.UI.OpenPopup, OpenPopupOnMessage<Dragon>);
		Game.instance.messageBroker.SubscribeTo<System.Enum, Unit, Dragon>(Messages.UI.OpenPopup, OpenPopupOnMessage<Unit, Dragon>);
		Game.instance.messageBroker.SubscribeTo<Enum, Unit, ConfirmationPopupData>(Messages.UI.OpenPopup, OpenPopupOnMessage<Unit, ConfirmationPopupData>);


		Game.instance.messageBroker.SubscribeTo<UIPopupBase>(Messages.UI.ClosePopup, ClosePopup);
		Game.instance.messageBroker.SubscribeTo<Enum>(Messages.UI.ClosePopup, ClosePopupOnMessage);

//		Game.instance.messageBroker.SubscribeTo(Messages.UI.EnablePopup, EnableTopPopup);
//		Game.instance.messageBroker.SubscribeTo(Messages.UI.DisablePopup, DisableTopPopup);

		//Game.instance.messageBroker.SubscribeTo<System.Enum>(Messages.UI.EnablePopup, SetInputEnabled);
		//Game.instance.messageBroker.SubscribeTo<System.Enum>(Messages.UI.DisablePopup, SetInputDisabled);

	}

	public void SetInputDisabled(Enum message)
	{
		if (screensByID.ContainsKey(message))
		{
			screensByID[message].DisableInput();;
		}

		if (popupsByID.ContainsKey(message))
		{
			popupsByID[message].DisableInput();;
		}
	}

	public void SetInputEnabled(Enum message)
	{
		if (screensByID.ContainsKey(message))
		{
			screensByID[message].EnableInput();;
		}

		if (popupsByID.ContainsKey(message))
		{
			popupsByID[message].EnableInput();;
		}
	}

	public void DisableTopPopup()
	{
		topPopup.DisableInput();
	}

	public void EnableTopPopup()
	{
		topPopup.EnableInput();
	}

	public void OpenCurrentStateScreen(GameStateMachine.State state)
	{
		OpenScreen(stateScreens[state]);
	}

	public bool IsPopupOpen(UIPopupBase popup)
	{
		return activePopups.Contains(popup);
	}

	protected void OpenScreen(UIScreen screen)
	{
		screen.gameObject.SetActive(true);
		CloseActiveScreen();	
		activeScreen = screen;
	}

	public void OpenScreen(Enum message)
	{
		if (screensByID.ContainsKey(message))
		{
			OpenScreen(screensByID[message]);
		}
	}

	public void OpenPopup(UIPopupBase popup)
	{
		topPopup = popup;

		if (activePopups.Contains(popup)) return;

		popup.gameObject.SetActive(true);
		activePopups.Push(popup);
	}


//--------------- GENERICS ----------------------------

	public void OpenPopupOnMessage(Enum message)
	{
		if (popupsByID.ContainsKey(message))
		{
			popupsByID[message].Init();
			OpenPopup(popupsByID[message]);
		}
	}

	public void OpenPopupOnMessage<T>(Enum message, T param)
	{
		if (popupsByID.ContainsKey(message))
		{
			Debug.Assert(popupsByID[message] is UIPopup<T>, "The popup type doesn't match the parameter you are trying to pass", popupsByID[message]);

			(popupsByID[message] as UIPopup<T>).Init(param);
			OpenPopup(popupsByID[message]);
		}
	}

	public void OpenPopupOnMessage<T, U>(Enum message, T t, U u)
	{
		if (popupsByID.ContainsKey(message))
		{
			Debug.Assert(popupsByID[message] is UIPopup<T, U>, "The popup type doesn't match the parameter you are trying to pass", popupsByID[message]);

			(popupsByID[message] as UIPopup<T, U>).Init(t, u);
			OpenPopup(popupsByID[message]);
		}
	}

	public void OpenPopupOnMessage<T, U, V>(Enum message, T t, U u, V v)
	{
		if (popupsByID.ContainsKey(message))
		{
			Debug.Assert(popupsByID[message] is UIPopup<T, U, V>, "The popup type doesn't match the parameter you are trying to pass", popupsByID[message]);

			(popupsByID[message] as UIPopup<T, U, V>).Init(t, u, v);
			OpenPopup(popupsByID[message]);
		}
	}

	public void OpenPopupOnMessage<T, U, V, W>(Enum message, T t, U u, V v, W w)
	{
		if (popupsByID.ContainsKey(message))
		{
			Debug.Assert(popupsByID[message] is UIPopup<T, U, V, W>, "The popup type doesn't match the parameter you are trying to pass", popupsByID[message]);

			(popupsByID[message] as UIPopup<T, U, V, W>).Init(t, u, v, w);
			OpenPopup(popupsByID[message]);
		}
	}

//---------------------------------

	public void ClosePopupOnMessage(Enum message)
	{
		if (popupsByID.ContainsKey(message))
		{
			ClosePopup(popupsByID[message]);
		}
	}

	/// <summary>
	/// Closes all popups in the stack until the requested popup has been closed
	/// </summary>
	/// <param name="popup">Popup.</param>
	public void ClosePopup(UIPopupBase popup)
	{
//		Debug.Log("Close popup", popup);
		if (activePopups.Contains(popup))
		{
			UIPopupBase lastPopup = null;

			do
			{
				lastPopup = activePopups.Pop();
				lastPopup.TransitionOut();


			}
			while (lastPopup != popup);

			if (activePopups.Count > 0) 
			{
				topPopup = activePopups.Peek();
			}
			else
			{
				topPopup = null;
			}

		}
		else
		{
			Debug.LogError("You're trying to close a popup that isn't in the stack!", popup);
		}
	}

	protected void CloseActiveScreen()
	{
		if (activeScreen != null)
		{
			activeScreen.TransitionOut();
		}
	}

	/// <summary>
	/// Closes the top popup in the stack
	/// </summary>
	public void CloseActivePopup()
	{
		if (activePopups.Count > 0)
		{
			activePopups.Pop().TransitionOut();
		}

		topPopup = activePopups.Peek();
	}
}

