﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameScreen : UIScreen {

	public RectTransform ScoreMeter;
	public RectTransform Player1ScoreBar;
	public RectTransform Player2ScoreBar;

	public RectTransform MagicMeter;
	public RectTransform MagicBar;

	public Image portrait;
	public Image banner;

	public Text MagicText;

	public Button TurnEndButton;

	public override void Awake ()
	{
		base.Awake ();

		SubscribeToMessages();
	}

	/*public override void OnDisable ()
	{
		base.OnDisable ();

		Player1ScoreBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);
		Player2ScoreBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, 0);


	}*/

	public void CancelAction ()
	{
		Game.instance.messageBroker.SendMessage(Messages.Input.ActionCancelClicked);
	}

	public void SubscribeToMessages()
	{
		Game.instance.messageBroker.SubscribeTo<Dragon>(Messages.Events.MagicUsed, UpdateMagicMeter);
		Game.instance.messageBroker.SubscribeTo<Dragon>(Messages.UI.UpdatePlayerDisplay, UpdatePlayerDisplay);
		Game.instance.messageBroker.SubscribeTo<Player, Player>(Messages.UI.UpdateRealmMeter, UpdateRealmMeter);

	}

	public void UnsubscribeFromMessages()
	{
		Game.instance.messageBroker.UnsubscribeFrom<Dragon>(Messages.Events.MagicUsed, UpdateMagicMeter);
		Game.instance.messageBroker.UnsubscribeFrom<Player, Player>(Messages.UI.UpdateRealmMeter, UpdateRealmMeter);

	}

	public void UpdatePlayerDisplay(Dragon newPlayerDragon)
	{
		portrait.sprite = newPlayerDragon.data.dragonIcon;
		banner.sprite = newPlayerDragon.data.mainBanner;

		UpdateMagicMeter(newPlayerDragon);

		TurnEndButton.interactable = true;
	}

	public void UpdateRealmMeter(Player player1, Player player2)
	{		
		Debug.Log(player1.dragon.alliedSettlements.Count);

		Player1ScoreBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)player1.dragon.alliedSettlements.Count/Map.totalSettlements*ScoreMeter.sizeDelta.x);
		Player2ScoreBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)player2.dragon.alliedSettlements.Count/Map.totalSettlements*ScoreMeter.sizeDelta.x);
	}

	public void UpdateMagicMeter(Dragon currentPlayerDragon)
	{
		MagicText.text = string.Format("{0}/{1}", currentPlayerDragon.magic, Dragon.maxMagic);
		MagicBar.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, (float)currentPlayerDragon.magic/Dragon.maxMagic*MagicMeter.sizeDelta.x);

	}

	public void EndPlayerTurnPressed()
	{
		TurnEndButton.interactable = false;

		var popupData = new ConfirmationPopupData("End Turn", "Are you sure you want to end your turn?", Messages.UI.Confirm, Messages.UI.Cancel);

		Game.instance.messageBroker.SubscribeTo(Messages.UI.Confirm, ConfirmEndTurn);
		Game.instance.messageBroker.SubscribeTo(Messages.UI.Cancel, CancelEndTurn);

		Game.instance.messageBroker.SendMessage<System.Enum, ConfirmationPopupData>(Messages.UI.OpenPopup, ScreenManager.PopupID.Confirm, popupData);

	}

	public void ConfirmEndTurn()
	{
		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.Confirm, ConfirmEndTurn);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.Cancel, CancelEndTurn);


		Game.instance.messageBroker.SendMessage(Messages.Events.EndTurn);

	}

	public void CancelEndTurn()
	{
		TurnEndButton.interactable = true;

		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.Confirm, ConfirmEndTurn);
		Game.instance.messageBroker.UnsubscribeFrom(Messages.UI.Cancel, CancelEndTurn);

	}

}
