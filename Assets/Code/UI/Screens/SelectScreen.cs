﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectScreen : UIScreen {

	/*public Toggle MaudeButton;
	public Toggle EvelynButton;

	public Text MaudeText;
	public Text EvelynText;*/

	public Game.DragonId[] dragonIds;

	public Sprite P1Sprite;
	public Sprite P2Sprite;

	public Image MaudeIcon;
	public Image EvelynIcon;

	public override void Awake ()
	{
		base.Awake ();

		dragonIds = new Game.DragonId[2];

	}

	public override void OnEnable ()
	{
		base.OnEnable ();

		dragonIds[0] = Game.DragonId.Maude;
		dragonIds[1] = Game.DragonId.Evelyn;

		/*MaudeButton.isOn = true;
		MaudeButton.Select();*/

		MaudeIcon.sprite = P1Sprite;
		EvelynIcon.sprite = P2Sprite;

	}


	public void OnToggleButtonClick()
	{
		if (dragonIds[0] == Game.DragonId.Maude)
		{
			dragonIds[0] = Game.DragonId.Evelyn;
			dragonIds[1] = Game.DragonId.Maude;

			MaudeIcon.sprite = P2Sprite;
			EvelynIcon.sprite = P1Sprite;

		}
		else
		{
			dragonIds[0] = Game.DragonId.Maude;
			dragonIds[1] = Game.DragonId.Evelyn;

			MaudeIcon.sprite = P1Sprite;
			EvelynIcon.sprite = P2Sprite;
		}
	}

	/*public void OnMaudeButtonClick(bool toggledOn)
	{
		if (toggledOn)
		{
			MaudeButton.animator.SetTrigger(MaudeButton.animationTriggers.highlightedTrigger);

			MaudeText.text = "Player 1";
			EvelynText.text = "Player 2";

			dragonIds[0] = Game.DragonId.Maude;
			dragonIds[1] = Game.DragonId.Evelyn;

		}
	}

	public void OnEvelynButtonClick(bool toggleOn)
	{
		if (toggleOn)
		{
			EvelynButton.animator.SetTrigger(EvelynButton.animationTriggers.highlightedTrigger);

			MaudeText.text = "Player 2";
			EvelynText.text = "Player 1";

			dragonIds[0] = Game.DragonId.Evelyn;
			dragonIds[1] = Game.DragonId.Maude;
		}
	}*/

	public void GoNext()
	{
		Game.instance.messageBroker.SendMessage<Game.DragonId[]>(Messages.Settings.SetPlayers, dragonIds);
		Game.instance.messageBroker.SendMessage<System.Enum>(Messages.UI.Confirm, this.id);
	}

	public void GoBack()
	{
		Game.instance.messageBroker.SendMessage<System.Enum>(Messages.UI.Cancel, this.id);
	}
}
