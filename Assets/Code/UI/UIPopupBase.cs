﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

/*public class PopupData<T>
{
	public T t;

	public PopupData(T _t)
	{
		t = _t;
	}
}*/

public class UIPopup<T>:UIPopupBase
{
	public virtual void Init(T u)
	{
	}
}

public class UIPopup<T, U>:UIPopupBase
{
	public virtual void Init(T t, U u)
	{
	}
}

public class UIPopup<T, U, V>:UIPopupBase
{
	public virtual void Init(T t, U u, V v)
	{
	}
}

public class UIPopup<T, U, V, W>:UIPopupBase
{
	public virtual void Init(T t, U u, V v, W w)
	{
	}
}

public class UIPopupBase : UIObject
{

	public ScreenManager.PopupID id;

	public Text Title;
	public Text Body;

	protected Animator animator;

	protected Vector3? worldPos = null;

	public override void Awake()
	{
		base.Awake();

		animator = GetComponent<Animator>();

	}


	public override void OnEnable ()
	{
		base.OnEnable ();

		if (rectTransform == null)
			rectTransform = (RectTransform)transform;


	}

	public override void TransitionIn ()
	{
		base.TransitionIn ();

	}

	public virtual void Init()
	{
		if (rectTransform == null)
			rectTransform = (RectTransform)transform;
	}

	public virtual void Confirm()
	{

		Game.instance.messageBroker.SendMessage<UIPopupBase>(Messages.UI.ClosePopup, this);

	}

	public virtual void Cancel()
	{

		Game.instance.messageBroker.SendMessage<UIPopupBase>(Messages.UI.ClosePopup, this);

	}

	public virtual void ShowOverWorldPosition(Vector3 worldPos)
	{
		trackingRect.center = Camera.main.WorldToScreenPoint(worldPos);


		/*Debug.DrawLine(new Vector3(trackingRect.xMin, trackingRect.yMin), new Vector3(trackingRect.xMin, trackingRect.yMax));
		Debug.DrawLine(new Vector3(trackingRect.xMin, trackingRect.yMin), new Vector3(trackingRect.xMax, trackingRect.yMin));
		Debug.DrawLine(new Vector3(trackingRect.xMax, trackingRect.yMin), new Vector3(trackingRect.xMax, trackingRect.yMax));
		Debug.DrawLine(new Vector3(trackingRect.xMin, trackingRect.yMax), new Vector3(trackingRect.xMax, trackingRect.yMax));*/

		var clampedScreenPos = Camera.main.WorldToScreenPoint(worldPos);

		clampedScreenPos.x = Mathf.Clamp(clampedScreenPos.x, 0+trackingRect.width/2, Screen.width-trackingRect.width/2);
		clampedScreenPos.y = Mathf.Clamp(clampedScreenPos.y, 0+trackingRect.height/2, Screen.height-trackingRect.height/2);

		//if (!docked)
		{

		Vector2 newAnchoredPos;

		RectTransformUtility.ScreenPointToLocalPointInRectangle((RectTransform)rectTransform.parent, clampedScreenPos, null, out newAnchoredPos);

		rectTransform.anchoredPosition = newAnchoredPos;
		}
	}

	public virtual void ShowOverWorldPosition(Vector3 worldPos, RectTransform childTransform)
	{

		trackingRect.center = Camera.main.WorldToScreenPoint(worldPos);

		if (!docked)
		{
			Vector2 newAnchoredPos;

			RectTransformUtility.ScreenPointToLocalPointInRectangle(rectTransform, Camera.main.WorldToScreenPoint(worldPos), null, out newAnchoredPos);

			childTransform.anchoredPosition = newAnchoredPos;
		}
	}


	public override void LateUpdate ()
	{
		base.LateUpdate ();

		if (worldPos != null)
			ShowOverWorldPosition((Vector3)worldPos);

	}

	public override void Undock ()
	{
		base.Undock ();

		if (worldPos != null)
			ShowOverWorldPosition((Vector3)worldPos);
	}
}


