﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SettlementPopupMain : UIPopup<Structure, Dragon> {

	/*public Text type;
	public Text produces;
	public Text requires;
	public Text prosperity;
	public Text alliance;

	public CanvasGroup buttons;*/

	public Button TradeRouteButton;

	public Button SpectreButton;
	public Button EnvoyButton;
	public Button PawnButton;

	public Image requiresImage;
	public GameObject requestBubble;

	public Text numSpectres;
	public Text numEnvoys;
	public Text numPawns;

	private Structure currentStructure;
	private Dragon activeDragon;

	private Settlement currentSettlement;

	public ResourceData resourceData;

	public UnitData envoyData;
	public UnitData spectreData;
	public UnitData pawnData;

	public CanvasGroup pointer;
	public RectTransform pointerImg;
	public CanvasGroup main;

	public override void OnEnable()
	{
		base.OnEnable();
	}

	public override void Init (Structure s, Dragon d)
	{
		base.Init();
		
		currentStructure = s;
		activeDragon = d;
		RefreshDisplay();
	}

	public void RefreshDisplay()
	{

		worldPos = currentStructure.transform.position;


		if (currentStructure is Settlement)
		{
			currentSettlement = currentStructure as Settlement;

			if (currentSettlement.CanSeeQuest(activeDragon))
			{

				requestBubble.SetActive(true);

				requiresImage.sprite = resourceData.GetSpriteByType(currentSettlement.resourceRequired, false);
			}
			else
			{
				requestBubble.SetActive(false);


			}
		}
		else
		{
			requestBubble.SetActive(false);

		}
		
		RefreshButtonVisibility();

	}

	public void RefreshButtonVisibility()
	{

		TradeRouteButton.gameObject.SetActive(currentStructure is Settlement);
		TradeRouteButton.interactable = (currentStructure.queen == activeDragon);

		int spectres = 0;
		int envoys = 0;
		int pawns = 0;

		bool spectresAvailable = currentStructure.canBuildUnits && activeDragon.magic >= spectreData.creationCost && currentStructure.queen == activeDragon;
		bool envoysAvailable = currentStructure.canBuildUnits && activeDragon.magic >= envoyData.creationCost && currentStructure.queen == activeDragon;
		bool pawnsAvailable = currentStructure.canBuildUnits && activeDragon.magic >= pawnData.creationCost && currentStructure.queen == activeDragon;

		envoys = currentStructure.AvailableUnitsOfType(UnitData.UnitType.Envoy, activeDragon);
		spectres = currentStructure.AvailableUnitsOfType(UnitData.UnitType.Spectre, activeDragon);

		if (!envoysAvailable)
		envoysAvailable = envoys > 0;

		if (!spectresAvailable)
		spectresAvailable = spectres > 0;

		SpectreButton.interactable = spectresAvailable;
		EnvoyButton.interactable = envoysAvailable;
		PawnButton.interactable = pawnsAvailable;

		PawnButton.gameObject.SetActive(pawnsAvailable);

		if (!currentStructure.canBuildUnits)
		{
		numEnvoys.text = "x" + envoys;
		numSpectres.text = "x" + spectres;
		numPawns.text = "x" + pawns;
		}
		else
		{
			
			numEnvoys.text =  envoys > 0 ? "x" + envoys: "Build";
			numSpectres.text = spectres > 0 ? "x" + spectres: "Build";
			numPawns.text = "Build";
		}

	}

	public void SetUnit(string unitType)
	{
		UnitData.UnitType type = UnitData.UnitTypesByString[unitType];

		Game.instance.messageBroker.SendMessage<Unit>(Messages.Input.UnitClicked, currentStructure.SelectCurrentUnit(type));

	}

	public void SetTradeRoute()
	{
		Game.instance.messageBroker.SendMessage(Messages.Input.SetTradeRouteClicked);
	}

	public override void LateUpdate ()
	{

		base.LateUpdate();

		bool visible = IsFullyVisible();

		if (!visible && main.alpha == 1)
		{
			main.alpha = 0;
			pointer.alpha = 1;
		}
		else if (visible && main.alpha == 0)
		{
			main.alpha = 1;
			pointer.alpha = 0;
		}

		UpdatePointer();
	}

	private void UpdatePointer()
	{
		//TODO: add something to this that shows the actual settlement (second camera + blit to quad?)
		// and also, what it requires!

		RectTransform pointerTrans = (RectTransform)pointer.transform;

		pointer.transform.up = trackingRect.center - RectTransformUtility.WorldToScreenPoint(null, transform.position);

		pointerTrans.anchoredPosition = pointer.transform.up.normalized * (Mathf.Min(pointerTrans.rect.width/2, pointerTrans.rect.height/2) - pointerImg.rect.width/2);
	}

}
