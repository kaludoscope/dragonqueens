﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class ConfirmationPopupData
{
	public string title;
	public string body;

	public Enum confirmMessage;
	public Enum cancelMessage;

	public Vector3? worldPosition;

	public ConfirmationPopupData(string t, string b, Enum confirm = null, Enum cancel = null, Vector3? worldPos = null)
	{
		title = t;
		body = b;
		confirmMessage = confirm;
		cancelMessage = cancel;
		worldPosition = worldPos;
	}
}

public class ConfirmationPopup : UIPopup<ConfirmationPopupData>
{

	//modal dialogue for confirming stuff

	private ConfirmationPopupData popupData;


	public override void Init (ConfirmationPopupData _popupData)
	{
		popupData = _popupData;

		if (Title != null)
		Title.text = popupData.title;

		if (Body != null)
		Body.text = popupData.body;

		worldPos = popupData.worldPosition;
	}

	public override void Cancel ()
	{
		base.Cancel ();

		if (popupData.cancelMessage != null)
		{
			Game.instance.messageBroker.SendMessage(popupData.cancelMessage);
		}

	}

	public override void Confirm ()
	{
		base.Confirm ();

		if (popupData.confirmMessage != null)
		{
			Game.instance.messageBroker.SendMessage(popupData.confirmMessage);
		}

	}

}

