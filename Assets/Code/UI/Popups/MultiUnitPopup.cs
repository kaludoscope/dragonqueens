﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MultiUnitPopup : UIPopup<Unit, ConfirmationPopupData>
{
	private ConfirmationPopupData popupData;

	private Unit currentUnit;

	public Image unitIcon;
	public Text number;

	private int numUnitsSelected;

	public Button incrementButton;
	public Button decrementButton;

	private int availableUnits;

	public override void Init (Unit u, ConfirmationPopupData _popupData)
	{
		popupData = _popupData;

		if (Title != null)
			Title.text = popupData.title;

		if (Body != null)
			Body.text = popupData.body;
		
		worldPos = popupData.worldPosition;

		currentUnit = u;

		numUnitsSelected = 1;
		number.text = numUnitsSelected.ToString();

		availableUnits = u.numAvailableSiblings;

		decrementButton.interactable = false;
		incrementButton.interactable = true;
	}

	public void IncrementCount()
	{
		Debug.Log("clickyup " + numUnitsSelected);

		numUnitsSelected ++;

		number.text = numUnitsSelected.ToString();


		if (numUnitsSelected == availableUnits)
			incrementButton.interactable = false;

		if (!decrementButton.interactable)
			decrementButton.interactable = true;
	}
	
	public void DecrementCount()
	{
		Debug.Log("clickydown " + numUnitsSelected);

		numUnitsSelected --;

		number.text = numUnitsSelected.ToString();

		if (!incrementButton.interactable)
			incrementButton.interactable = true;

		if (numUnitsSelected == 1)
			decrementButton.interactable = false;
		
	}

	public override void Cancel ()
	{
		base.Cancel ();

		if (popupData.cancelMessage != null)
		{
			Game.instance.messageBroker.SendMessage(popupData.cancelMessage);
		}

	}

	public override void Confirm ()
	{
		base.Confirm ();

		if (popupData.confirmMessage != null)
		{
			Game.instance.messageBroker.SendMessage<int>(popupData.confirmMessage, numUnitsSelected);
		}

	}
}

