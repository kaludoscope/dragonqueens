﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]

public class UIObject : MonoBehaviour
{
	public GameStateMachine.State LinkedState = GameStateMachine.State.None;

	[SerializeField]
	protected bool transitionComplete = true;

	protected CanvasGroup topCanvasGroup;

	protected bool docked = false;


	protected RectTransform rectTransform;

	protected Rect trackingRect;

	protected Canvas rootCanvas;

	public virtual void OnEnable()
	{
		topCanvasGroup.interactable = true;
		TransitionIn();
	}

	public virtual void OnDisable()
	{
	}

	public virtual void Awake()
	{
		topCanvasGroup = GetComponent<CanvasGroup>();

		rectTransform = (RectTransform)transform;

		rootCanvas = GetComponentInParent<Canvas>();

		trackingRect = RectTransformUtility.PixelAdjustRect(rectTransform, rootCanvas);
	}

	// Use this for initialization
	public virtual void Start ()
	{
	
	}
	
	// Update is called once per frame
	public virtual void Update ()
	{
	
	}
		
	public virtual void LateUpdate()
	{

	}

	public virtual void TransitionIn()
	{
		
	}

	public virtual void TransitionOut()
	{
		gameObject.SetActive(false);
		//StartCoroutine(WaitForTransitionOut());
	}

	public virtual IEnumerator WaitforTransitionIn()
	{
		while(!transitionComplete)
		{
			yield return null;
		}
	}

	public virtual IEnumerator WaitForTransitionOut()
	{
		while(!transitionComplete)
		{
			yield return null;
		}

		//This will go in its own function eventually
		gameObject.SetActive(false);
	}

	public virtual void DisableInput()
	{
		topCanvasGroup.interactable = false;
	}

	public virtual void EnableInput()
	{
		topCanvasGroup.interactable = true;

	}

	public virtual bool IsFullyVisible()
	{
		bool allCornersVisible = 	(rootCanvas.pixelRect.Contains(new Vector2(trackingRect.xMax, trackingRect.yMin)) &&
									rootCanvas.pixelRect.Contains(new Vector2(trackingRect.xMax, trackingRect.yMax)) &&
									rootCanvas.pixelRect.Contains(new Vector2(trackingRect.xMin, trackingRect.yMin)) &&
									rootCanvas.pixelRect.Contains(new Vector2(trackingRect.xMin, trackingRect.yMax)));


		return allCornersVisible;

	}

	public bool IsOffScreen()
	{
		return (!rootCanvas.pixelRect.Contains(new Vector2(trackingRect.xMax, trackingRect.yMin)) &&
			!rootCanvas.pixelRect.Contains(new Vector2(trackingRect.xMax, trackingRect.yMax)) &&
			!rootCanvas.pixelRect.Contains(new Vector2(trackingRect.xMin, trackingRect.yMin)) &&
			!rootCanvas.pixelRect.Contains(new Vector2(trackingRect.xMin, trackingRect.yMax)));
	}

	public virtual void DockToNearestScreenAnchor()
	{
		var nearestPoint = Game.instance.screenAnchorData.screenAnchors[0];

		var dist = (Game.instance.screenAnchorData.screenAnchors[0].pos - trackingRect.center).magnitude;

		var nearestDist = dist;

//		Debug.Log("TRACKING RECT CENTER POSITION: " + trackingRect.center); 
//		Debug.Log(nearestPoint.presetName + " " + nearestDist);



		for (int i = 1; i < Game.instance.screenAnchorData.screenAnchors.Length; i++)
		{
			dist = (Game.instance.screenAnchorData.screenAnchors[i].pos - trackingRect.center).magnitude;
			Debug.Log(Game.instance.screenAnchorData.screenAnchors[i].presetName + " " + dist);

			if (dist < nearestDist)
			{
				nearestDist = dist;
				nearestPoint = Game.instance.screenAnchorData.screenAnchors[i];
			}
		
		}

		//Debug.Log(nearestPoint.presetName);

		Dock(nearestPoint.presetName);
	}

	public virtual void Dock(AnchorPresets NearestAnchor)
	{
		docked = true;

		RectTransformExtensions.SetAnchor(rectTransform, NearestAnchor);
		RectTransformExtensions.SetPivot(rectTransform, NearestAnchor);

		rectTransform.anchoredPosition = Vector2.zero;

	}

	public virtual void Undock()
	{
		docked = false;


		RectTransformExtensions.SetAnchor(rectTransform, AnchorPresets.MiddleCenter);
		RectTransformExtensions.SetPivot(rectTransform, AnchorPresets.MiddleCenter);


	}
}

