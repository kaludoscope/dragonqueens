﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Pawn : Unit {

	public override void GoTo (PathNode node)
	{
		base.GoTo (node);
	}


	public void OnMouseUpAsButton()
	{
		Game.instance.messageBroker.SendMessage<Unit>(Messages.Input.UnitClicked, this);
	}
		
}
