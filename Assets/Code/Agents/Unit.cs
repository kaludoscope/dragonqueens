﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(PathFollower))]
[RequireComponent(typeof(PathFinder))]

public class Unit : MonoBehaviour {

	public Dragon queen;

	public Structure currentStructure 
	{
		get 
		{
			return location.connectedStructure;
		}
		private set{}
	}
	public PathNode location;

	public UnitData unitData;

	protected PathFollower pathFollower;
	protected PathFinder pathFinder;

	/// <summary>
	/// Has the unit moved in this turn - can only be moved once per turn and cannot be destroyed in the same turn as moving
	/// </summary>
	public bool hasMoved;

	public SpriteRenderer bannerSprite;

	public bool justCreated = true;

	public int numAvailableSiblings
	{
		get 
		{
			return location.numAvailableUnits(this);
		}

		private set {}
	}

	public UnitData.UnitType UnitType {
		get
		{
			return unitData.type;
		}
	}

	public virtual void Init(Dragon q, Structure l)
	{

		queen = q;
		currentStructure = l;

		bannerSprite.sprite = q.data.unitBanner;

		pathFollower = GetComponent<PathFollower>();
		pathFinder = GetComponent<PathFinder>();

		location = l.pathNode;

		pathFinder.Setup(l.pathNode, unitData.moveDistance);

		Game.instance.messageBroker.SubscribeTo(Messages.Input.ActionCancelClicked, OnActionCancel);
	}

	public void ShowAvailableDestinations()
	{
		pathFinder.HighlightAvailableDestinations(false, null, this.UnitType == UnitData.UnitType.Pawn, true);

	}

	public virtual void GoTo(PathNode node)
	{
		//when we confirm the first move for the unit, we deduct the build cost and so on from the queen
		if (justCreated) 
		{
			queen.OnUnitBuild(this);
			justCreated = false;

			Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.ActionCancelClicked, OnActionCancel);

		}

		location.RemoveUnit(this);

		/*if (currentStructure != null)
			currentStructure.RemoveUnit(this);

		currentStructure = node.GetComponent<Structure>();

		if (currentStructure != null)
			currentStructure.AddUnit(this);*/

		hasMoved = true;

		location = node;

		location.AddUnit(this);
	
		pathFollower.SetRoute(pathFinder.GetRouteTo(node));

		pathFinder.SetNewStartNode(node);

	}

	public virtual void DestroySelf()
	{
		/*if (currentStructure != null)
			currentStructure.RemoveUnit(this);*/

		location.RemoveUnit(this);
		
		queen.OnUnitDestroy(this);

		Destroy(gameObject);
	}

	public virtual void OnActionCancel()
	{
		if (justCreated)
		{
			DestroySelf();
			Game.instance.messageBroker.UnsubscribeFrom(Messages.Input.ActionCancelClicked, OnActionCancel);

		}
	}

	public Unit [] GetSiblings(int count)
	{
		Unit [] units = location.GetUnits(this, count);

		for (int i = 0; i < units.Length; i++)
		{
			units[i].CopyAvailableDestinations(this.pathFinder);
		}

		return units;
	}

	public bool HasSiblings()
	{
		return location.HasMultipleUnits(this);
	}

	public void CopyAvailableDestinations(PathFinder source)
	{
		pathFinder.CopyCheckedNodes(source);
	}
}
