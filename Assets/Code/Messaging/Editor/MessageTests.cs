﻿using System;
using System.Collections.Generic;
using System.Threading;
using NUnit.Framework;
using UnityEngine;
using NSubstitute;

namespace UnityTest
{
	public enum TestMessage
	{
		Message1, 
		Message2
	}

	public interface IMessageSubscriber
	{
		void TestMessageDelegate();
		void TestMessageDelegateString(string s);
	}

	[TestFixture]
	[Category("Messaging Tests")]
	internal class MessageTests
	{
		/// <summary>
		/// Method under test: MessageBroker.SubscribeTo
		/// </summary>
		[Test]
		public void ShouldContainSubscriptionToMessage()
		{
			//Arrange
			var subscriberMock = Substitute.For<IMessageSubscriber>(); 

			var message = TestMessage.Message1;

			MessageBroker broker = new MessageBroker();

			//Act
			broker.SubscribeTo(message, subscriberMock.TestMessageDelegate);

			//Assert
			Assert.That(broker.ContainsSubscription(message, subscriberMock.TestMessageDelegate));
		}

		/// <summary>
		/// Method under test: MessageBroker.SubscribeTo
		/// </summary>
		[Test]
		public void ShouldContainSubscriptionWithParamsToMessage()
		{
			//Arrange

			var subscriberMock = Substitute.For<IMessageSubscriber>(); 

			var message = TestMessage.Message1;

			MessageBroker broker = new MessageBroker();

			//Act
			broker.SubscribeTo<string>(message, subscriberMock.TestMessageDelegateString);

			//Assert
			Assert.That(broker.ContainsSubscription<string>(message, subscriberMock.TestMessageDelegateString));
		}



		/// <summary>
		/// Method under test: MessageBroker.UnsubscribeFrom
		/// </summary>
		[Test]
		public void ShouldNotContainSubscriptionToMessage()
		{
			var subscriberMock = Substitute.For<IMessageSubscriber>();

			var message = TestMessage.Message1;

			MessageBroker broker = new MessageBroker();

			//Act
			broker.SubscribeTo(message, subscriberMock.TestMessageDelegate);

			broker.UnsubscribeFrom(message, subscriberMock.TestMessageDelegate);


			//Assert
			Assert.That(!broker.ContainsSubscription(message, subscriberMock.TestMessageDelegate));
		}



		[Test]
		public void ShouldInvokeDelegateOnMessage()
		{
			//Arrange

			var subscriberMock = Substitute.For<IMessageSubscriber>(); 

			var message = TestMessage.Message1;

			MessageBroker broker = new MessageBroker();

			//Act
			broker.SubscribeTo(message, subscriberMock.TestMessageDelegate);

			broker.SendMessage(message);

			subscriberMock.Received().TestMessageDelegate();
		}

		[Test]
		public void ShouldInvokeDelegateWithParameterOnMessage()
		{
			//Arrange

			var subscriberMock = Substitute.For<IMessageSubscriber>(); 

			var message = TestMessage.Message1;

			MessageBroker broker = new MessageBroker();

			//Act
			broker.SubscribeTo<string>(message, subscriberMock.TestMessageDelegateString);

			broker.SendMessage<string>(message, "test");

			subscriberMock.Received().TestMessageDelegateString("test");
		}

		[Test]
		public void ShouldNotInvokeIncorrectDelegateOnMessage()
		{
			//Arrange

			var subscriberMock = Substitute.For<IMessageSubscriber>(); 

			var message1 = TestMessage.Message1;
			var message2 = TestMessage.Message2;

			MessageBroker broker = new MessageBroker();

			//Act
			broker.SubscribeTo(message1, subscriberMock.TestMessageDelegate);

			broker.SendMessage(message2);

			subscriberMock.DidNotReceive().TestMessageDelegate();
		}

	}
}


