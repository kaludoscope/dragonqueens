﻿
public abstract class Messages
{
	public enum Input
	{
		SettlementClicked,
		UnitClicked,
		SetTradeRouteClicked,
		MoveUnitClicked,
		DestroyUnitClicked,
		BuildUnitClicked,
		ActionCancelClicked,
		PathNodeClicked
	}

	public enum UI
	{
		OpenPopup,
		ClosePopup,
		OpenScreen,
		Confirm,
		Cancel,
		ConfirmDestination,
		CancelDestination,
		UpdateRealmMeter,
		HighlightRoute,
		UnhighlightRoute,
		UpdatePlayerDisplay,
		DisablePopup,
		EnablePopup

	}

	public enum Settings
	{
		SetPlayers
	}

	public enum Events
	{
		LoadLevel,
		StartPlay,

		SnapToCastle,
		SetDragEnabled,

		StartResourcePhase,
		MagicUsed,
		MagicGained,
		SettlementSelected,
		ResetToSettlementInteraction,
		ResetToPlayerAction,
		EndTurn,
		ResourceArrived,
		AllResourcesArrived,
		ClearAllHighlights,

		GameOver
	}

}