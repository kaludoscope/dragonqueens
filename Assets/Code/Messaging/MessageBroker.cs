﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Reflection;

//Message broker class
public class MessageBroker
{

	public Dictionary<Enum, List<Delegate>> activeSubscriptions;

	public MessageBroker()
	{
		activeSubscriptions = new Dictionary<Enum, List<Delegate>>();
	}


	//MESSAGE SUBSCRIPTION METHODS


	//------ BOILERPLATE
	public void SubscribeTo(Enum message, Action a)
	{
		_SubscribeTo(message, a);
	}

	public void SubscribeTo<T>(Enum message, Action<T> a)
	{
		_SubscribeTo(message, a);
	}

	public void SubscribeTo<T, U>(Enum message, Action<T, U> a)
	{
		_SubscribeTo(message, a);
	}

	public void SubscribeTo<T, U, V>(Enum message, Action<T, U, V> a)
	{
		_SubscribeTo(message, a);
	}

	public void SubscribeTo<T, U, V, W>(Enum message, Action<T, U, V, W> a)
	{
		_SubscribeTo(message, a);
	}
	//-------------

	private void _SubscribeTo(Enum message, Delegate d)
	{
		if (!activeSubscriptions.ContainsKey(message))
			activeSubscriptions.Add(message, new List<Delegate>());

		if (!activeSubscriptions[message].Contains(d))
			activeSubscriptions[message].Add(d);
	}

	// MESSAGE UNSUBSCRIPTION

	//---------BOILERPLATE
	public void UnsubscribeFrom(Enum message, Action a)
	{
		_UnsubscribeFrom(message, a);
	}

	public void UnsubscribeFrom<T>(Enum message, Action<T> a)
	{
		_UnsubscribeFrom(message, a);
	}

	public void UnsubscribeFrom<T, U>(Enum message, Action<T, U> a)
	{
		_UnsubscribeFrom(message, a);
	}

	public void UnsubscribeFrom<T, U, V>(Enum message, Action<T, U, V> a)
	{
		_UnsubscribeFrom(message, a);
	}

	public void UnsubscribeFrom<T, U, V, W>(Enum message, Action<T, U, V, W> a)
	{
		_UnsubscribeFrom(message, a);
	}
	//-----------

	private void _UnsubscribeFrom(Enum message, Delegate d)
	{
		if (activeSubscriptions.ContainsKey(message))
		{
			if (activeSubscriptions[message].Contains(d))
				activeSubscriptions[message].Remove(d);

			if (activeSubscriptions[message].Count == 0)
				activeSubscriptions.Remove(message);
		}
	}


	//NOTE: is it possible to unsubscribe an *objecct*?? it may not be a) possible easily or b) important

	//MESSAGE SENDING

	private List<Delegate> ActiveMessageSubscriptions(Enum message)
	{
		if (activeSubscriptions.ContainsKey(message))
		{
			if (activeSubscriptions[message].Count > 0)
			{
				return activeSubscriptions[message];
			}
		}

		return null;
	}


	//this should invoke any matching delegates it finds
	public void SendMessage(Enum message)
	{
		if  (!activeSubscriptions.ContainsKey(message)) return;

		List<Delegate> subs = new List<Delegate>(ActiveMessageSubscriptions(message));

		foreach (Delegate d in subs)
		{
			if (d is Action)
				(d as Action).Invoke();
		}
	}

	public void SendMessage<T>(Enum message, T t)
	{
		if  (!activeSubscriptions.ContainsKey(message)) return;

		List<Delegate> subs = new List<Delegate>(ActiveMessageSubscriptions(message));

		foreach (Delegate d in subs)
		{
			if (d is Action<T>)
				(d as Action<T>).Invoke(t);
		}
	}

	public void SendMessage<T, U>(Enum message, T t, U u)
	{
		if  (!activeSubscriptions.ContainsKey(message)) return;

		List<Delegate> subs = new List<Delegate>(ActiveMessageSubscriptions(message));

		foreach (Delegate d in subs)
		{
			if (d is Action<T, U>)
				(d as Action<T, U>).Invoke(t, u);
		}
	}

	public void SendMessage<T, U, V>(Enum message, T t, U u, V v)
	{
		if  (!activeSubscriptions.ContainsKey(message)) return;

		List<Delegate> subs = new List<Delegate>(ActiveMessageSubscriptions(message));

		foreach (Delegate d in subs)
		{
			if (d is Action<T, U, V>)
				(d as Action<T, U, V>).Invoke(t, u, v);
		}
	}
	public void SendMessage<T, U, V, W>(Enum message, T t, U u, V v, W w)
	{
		if  (!activeSubscriptions.ContainsKey(message)) return;

		List<Delegate> subs = new List<Delegate>(ActiveMessageSubscriptions(message));

		foreach (Delegate d in subs)
		{
			if (d is Action<T, U, V, W>)
				(d as Action<T, U, V, W>).Invoke(t,u , v, w );
		}
	}


	//Subscription checks

	public bool ContainsSubscription(Enum message, Action a)
	{
		return _ContainsSubscription(message, a);
	}

	public bool ContainsSubscription<T>(Enum message, Action<T> a)
	{
		return _ContainsSubscription(message, a);
	}

	public bool ContainsSubscription<T, U>(Enum message, Action<T, U> a)
	{
		return _ContainsSubscription(message, a);
	}

	public bool ContainsSubscription<T, U, V>(Enum message, Action<T, U, V> a)
	{
		return _ContainsSubscription(message, a);
	}

	public bool ContainsSubscription<T, U, V, W>(Enum message, Action<T, U, V, W> a)
	{
		return _ContainsSubscription(message, a);
	}

	private bool _ContainsSubscription(Enum message, Delegate d)
	{
		if (activeSubscriptions.ContainsKey(message))
		{
			return activeSubscriptions[message].Contains(d);
		}

		return false;

	}

	//TODO MessageBroker needs to be able to unsubscribe a listener function without caring what message it's looking for
}
