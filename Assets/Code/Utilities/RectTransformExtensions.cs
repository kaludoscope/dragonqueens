﻿using UnityEngine;

public enum AnchorPresets
{
	TopLeft,
	TopCenter,
	TopRight,

	MiddleLeft,
	MiddleCenter,
	MiddleRight,

	BottomLeft,
	BottomCenter,
	BottomRight,
	BottomStretch,

	VStretchLeft,
	VStretchRight,
	VStretchCenter,

	HStretchTop,
	HStretchMiddle,
	HStretchBottom,

	StretchAll
}

public enum PivotPresets
{
	TopLeft,
	TopCenter,
	TopRight,

	MiddleLeft,
	MiddleCenter,
	MiddleRight,

	BottomLeft,
	BottomCenter,
	BottomRight,
}


public static class RectTransformExtensions
{
	/*public static Vector2 TopLeft = new Vector2(0, 0);
	public static Vector2 TopCenter = new Vector2(0.5, 0);
	public static Vector2 TopRight = new Vector2(1, 0);

	public static Vector2 MiddleLeft = new Vector2(0, 0.5);
	public static Vector2 MiddleCenter = new Vector2(0.5, 0.5);
	public static Vector2 MiddleRight = new Vector2(1, 0.5);

	public static Vector2 BottomLeft = new Vector2(0, 1);
	public static Vector2 BottomCenter = new Vector2(0.5, 1);
	public static Vector2 BottomRight = new Vector2(1, 1);*/


	public static void SetAnchor(this RectTransform source, AnchorPresets align, int offsetX=0, int offsetY=0)
	{
		source.anchoredPosition = new Vector3(offsetX, offsetY, 0);

		switch (align)
		{
		case(AnchorPresets.TopLeft):
			{
				source.anchorMin = new Vector2(0, 1);
				source.anchorMax = new Vector2(0, 1);
				break;
			}
		case (AnchorPresets.TopCenter):
			{
				source.anchorMin = new Vector2(0.5f, 1);
				source.anchorMax = new Vector2(0.5f, 1);
				break;
			}
		case (AnchorPresets.TopRight):
			{
				source.anchorMin = new Vector2(1, 1);
				source.anchorMax = new Vector2(1, 1);
				break;
			}

		case (AnchorPresets.MiddleLeft):
			{
				source.anchorMin = new Vector2(0, 0.5f);
				source.anchorMax = new Vector2(0, 0.5f);
				break;
			}
		case (AnchorPresets.MiddleCenter):
			{
				source.anchorMin = new Vector2(0.5f, 0.5f);
				source.anchorMax = new Vector2(0.5f, 0.5f);
				break;
			}
		case (AnchorPresets.MiddleRight):
			{
				source.anchorMin = new Vector2(1, 0.5f);
				source.anchorMax = new Vector2(1, 0.5f);
				break;
			}

		case (AnchorPresets.BottomLeft):
			{
				source.anchorMin = new Vector2(0, 0);
				source.anchorMax = new Vector2(0, 0);
				break;
			}
		case (AnchorPresets.BottomCenter):
			{
				source.anchorMin = new Vector2(0.5f, 0);
				source.anchorMax = new Vector2(0.5f,0);
				break;
			}
		case (AnchorPresets.BottomRight):
			{
				source.anchorMin = new Vector2(1, 0);
				source.anchorMax = new Vector2(1, 0);
				break;
			}

		case (AnchorPresets.HStretchTop):
			{
				source.anchorMin = new Vector2(0, 1);
				source.anchorMax = new Vector2(1, 1);
				break;
			}
		case (AnchorPresets.HStretchMiddle):
			{
				source.anchorMin = new Vector2(0, 0.5f);
				source.anchorMax = new Vector2(1, 0.5f);
				break;
			}
		case (AnchorPresets.HStretchBottom):
			{
				source.anchorMin = new Vector2(0, 0);
				source.anchorMax = new Vector2(1, 0);
				break;
			}

		case (AnchorPresets.VStretchLeft):
			{
				source.anchorMin = new Vector2(0, 0);
				source.anchorMax = new Vector2(0, 1);
				break;
			}
		case (AnchorPresets.VStretchCenter):
			{
				source.anchorMin = new Vector2(0.5f, 0);
				source.anchorMax = new Vector2(0.5f, 1);
				break;
			}
		case (AnchorPresets.VStretchRight):
			{
				source.anchorMin = new Vector2(1, 0);
				source.anchorMax = new Vector2(1, 1);
				break;
			}

		case (AnchorPresets.StretchAll):
			{
				source.anchorMin = new Vector2(0, 0);
				source.anchorMax = new Vector2(1, 1);
				break;
			}
		}
	}

	public static void SetPivot(this RectTransform source, PivotPresets preset)
	{

		switch (preset)
		{
		case (PivotPresets.TopLeft):
			{
				source.pivot = new Vector2(0, 1);
				break;
			}
		case (PivotPresets.TopCenter):
			{
				source.pivot = new Vector2(0.5f, 1);
				break;
			}
		case (PivotPresets.TopRight):
			{
				source.pivot = new Vector2(1, 1);
				break;
			}

		case (PivotPresets.MiddleLeft):
			{
				source.pivot = new Vector2(0, 0.5f);
				break;
			}
		case (PivotPresets.MiddleCenter):
			{
				source.pivot = new Vector2(0.5f, 0.5f);
				break;
			}
		case (PivotPresets.MiddleRight):
			{
				source.pivot = new Vector2(1, 0.5f);
				break;
			}

		case (PivotPresets.BottomLeft):
			{
				source.pivot = new Vector2(0, 0);
				break;
			}
		case (PivotPresets.BottomCenter):
			{
				source.pivot = new Vector2(0.5f, 0);
				break;
			}
		case (PivotPresets.BottomRight):
			{
				source.pivot = new Vector2(1, 0);
				break;
			}
		}
	}

	public static void SetPivot(this RectTransform source, AnchorPresets preset)
	{

		switch (preset)
		{
		case (AnchorPresets.TopLeft):
			{
				source.pivot = new Vector2(0, 1);
				break;
			}
		case (AnchorPresets.TopCenter):
			{
				source.pivot = new Vector2(0.5f, 1);
				break;
			}
		case (AnchorPresets.TopRight):
			{
				source.pivot = new Vector2(1, 1);
				break;
			}

		case (AnchorPresets.MiddleLeft):
			{
				source.pivot = new Vector2(0, 0.5f);
				break;
			}
		case (AnchorPresets.MiddleCenter):
			{
				source.pivot = new Vector2(0.5f, 0.5f);
				break;
			}
		case (AnchorPresets.MiddleRight):
			{
				source.pivot = new Vector2(1, 0.5f);
				break;
			}

		case (AnchorPresets.BottomLeft):
			{
				source.pivot = new Vector2(0, 0);
				break;
			}
		case (AnchorPresets.BottomCenter):
			{
				source.pivot = new Vector2(0.5f, 0);
				break;
			}
		case (AnchorPresets.BottomRight):
			{
				source.pivot = new Vector2(1, 0);
				break;
			}
		}
	}
}