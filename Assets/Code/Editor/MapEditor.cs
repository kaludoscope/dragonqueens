﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;
using System.Collections;

[CustomEditor(typeof(Map))]
public class MapEditor : Editor
{
	//SerializedProperty lookAtPoint;

	private PathNode lastNode;

	public int toolbarInt;
	public string[] toolbarStrings = new string[] {"None", "Draw Roads", "Delete Roads"};

	public bool dragMode = false;

	private GameObject selectedObject = null;

	public enum EditMode
	{
		None,
		DrawRoads,
		DeleteRoads


	}

	public EditMode currentEditMode;

	public override void OnInspectorGUI()
	{
		DrawDefaultInspector();


		GUILayout.Label("Current edit mode");

		toolbarInt = GUILayout.Toolbar(toolbarInt, toolbarStrings);

		currentEditMode = (EditMode)toolbarInt;

		if (GUILayout.Button("Update Road positions"))
		{
			UpdateRoads();
		}

		GUILayout.Label("Selected object: " + ((selectedObject != null) ? selectedObject.ToString(): "none"));

	}

	public void UpdateRoads()
	{
		var pathSections = (target as Map).GetComponentsInChildren<PathSection>();

		foreach (PathSection sec in pathSections)
		{

			var lr = sec.GetComponent<LineRenderer>();

			Undo.RecordObject(lr, "Set line renderer positions");

			for (int i = 0; i < sec.nodes.Length; i++)
			{
				lr.SetPosition(i, sec.nodes[i].transform.position);

			}

			//lr.SetPosition(0, sec.nodes[0].transform.position);
			//lr.SetPosition(1, sec.no.transform.position);
			sec.transform.position = Vector3.Lerp(sec.nodes[0].transform.position, sec.nodes[sec.nodes.Length - 1].transform.position, 0.5f);
		}

	}

	public void OnSceneGUI()
	{

		Event e = Event.current;

		if (e.alt)
		{	
			dragMode = true;
			return;
		}


		if (e.type == EventType.Layout && currentEditMode != EditMode.None)
		{
			HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
			//Debug.Log("Capturing events");
			return;
		}

		/*if (e.type == EventType.mouseDown)
		{
			isClick = true;
		}

		if (e.type == EventType.mouseMove && isClick)
		{
			isClick = false;
		}*/

		if (e.type == EventType.mouseUp)
		{
			selectedObject = null;
			
			if (dragMode) 
			{	
				dragMode = false;
				return;
			}

			//GameObject
			/*Ray worldRay = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
			RaycastHit hitInfo;

			if (Physics.Raycast (worldRay, out hitInfo))
			{
				selectedObject = hitInfo.collider.gameObject;
				//Debug.Log(hitInfo.collider.gameObject.GetComponent<Settlement>());
			}*/

			selectedObject = HandleUtility.PickGameObject(Event.current.mousePosition, true);


			Structure structure;
			PathNode node;

			if (selectedObject == null) 
			{
				lastNode = null;
				Repaint();

				return;
			}

			structure  = selectedObject.GetComponentInChildren<Structure>();
			node = selectedObject.GetComponentInChildren<PathNode>();

			switch (currentEditMode)
			{

			case EditMode.DrawRoads:
				
				if (e.button == 0)
				{
				
					if ( structure != null)
					{

						/*logic flow:
						 * 
						 * add and/or get pathnode
						 * 
						 * if there is a previous node and not already connected
						 * set as connected nodes
						 * add pathsection object to this node
						 * add references
						 * */

						Debug.Log("Selected settlement " + structure);

						//lets see if it's a town or city
						if (structure.parentStructure != null)
						{
							selectedObject = structure.parentStructure.gameObject;
							node = selectedObject.GetComponent<PathNode>();

							Debug.Log("Selected parent structure " + structure.parentStructure); 

						}

						if (node == null)
						{
							//Undo.RecordObject(selectedObject, "add node to settlement");
							node = Undo.AddComponent<PathNode>(selectedObject) as PathNode;

							Debug.Log("Added path node to selected object");

						}
							
						if (lastNode != null && lastNode != node)
						{
							
							if (!node.connectedNodes.Contains(lastNode)) 
							{
								//these aren't connected!

								Undo.RecordObject(node, "Update current node");
								node.connectedNodes.Add(lastNode);

								Undo.RecordObject(lastNode, "Update last node");
								lastNode.connectedNodes.Add(node);

								GameObject pathSectionObject = new GameObject("RoadRenderer" + node.name + lastNode.name);
								Undo.RegisterCreatedObjectUndo(pathSectionObject, "Create path section GameObject");

								LineRenderer lr = Undo.AddComponent<LineRenderer>(pathSectionObject);



								//lr.material = roadMat;
								//Undo.RecordObject(pathSectionObject, "Set path section transform parent");
								//pathSectionObject.transform.SetParent(node.transform);
								Undo.SetTransformParent(pathSectionObject.transform, node.transform, "Set path section transform parent");


								Undo.RecordObject(lr, "Set line renderer positions");

								lr.SetPosition(0, node.transform.position);
								lr.SetPosition(1, lastNode.transform.position);
								pathSectionObject.transform.position = Vector3.Lerp(node.transform.position, lastNode.transform.position, 0.5f);


								PathSection pathSec = Undo.AddComponent<PathSection>(pathSectionObject);

								Undo.RecordObject(pathSec, "Init path section");
								pathSec.Init(node, lastNode, lr);


								Undo.RecordObject(lastNode, "Add path section to last node");
								lastNode.connectedSections.Add(pathSec);
								Undo.RecordObject(lastNode, "Add path section to current node");
								node.connectedSections.Add(pathSec);
							}



							Debug.Log("Updated path nodes");
						}

					}


					lastNode = node;


					/*Undo.RegisterFullObjectHierarchyUndo(Map.instance.gameObject, "update roads");

					Map.instance.Init();*/

					//Undo.RegisterFullObjectHierarchyUndo(Map.instance, "update roads");

					/*Undo.RecordObject(Map.instance, "update roads");
					Map.instance.Init();*/

						/*else
						{
							//instantiate roadnode prefab in map

							
						Plane groundPlane = new Plane(Vector3.forward, Vector3.zero);
						Ray ray = HandleUtility.GUIPointToWorldRay (Event.current.mousePosition);
						float rayDistance;

							if (groundPlane.Raycast(ray, out rayDistance))
							{
								var pathGO = (GameObject) PrefabUtility.InstantiatePrefab(Map.instance.roadPrefab);

								Undo.RegisterCreatedObjectUndo (pathGO, "Created RoadNode");

								pathGO.transform.position = ray.GetPoint(rayDistance);
								pathGO.transform.SetParent(Map.instance.transform);

								newNode = pathGO.GetComponent<PathNode>();
								newNode.connectedNodes.Add(lastNode);
								lastNode.connectedNodes.Add(newNode);

								lastNode = newNode;
							}

						}*/


					}
					/*else 
					{
						//end current road if not left click
						lastNode = null;
					}*/

				e.Use();
					

				Repaint();

				break;

			case EditMode.DeleteRoads:

				PathSection selectedSection = selectedObject.GetComponent<PathSection>();

				if (e.button == 0)
				{
					if (selectedSection != null)
					{
						DeletePathSection(selectedSection);
					}
					else if (node != null)
					{


						for (int i = node.connectedSections.Count - 1; i >= 0; i--)
						{

							DeletePathSection(node.connectedSections[i]);

						}

						node.connectedSections.Clear();

					}
				}

				break;

			}



		}

	}

	private void DeletePathSection(PathSection sec)
	{
		Undo.RecordObjects(sec.nodes, "remove node connections");

		sec.nodes[0].connectedNodes.Remove(sec.nodes[1]);
		sec.nodes[1].connectedNodes.Remove(sec.nodes[0]);

		sec.nodes[0].connectedSections.Remove(sec);
		sec.nodes[1].connectedSections.Remove(sec);

		Undo.DestroyObjectImmediate(sec.gameObject);
	}
}