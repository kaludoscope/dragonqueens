﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Dragon : MonoBehaviour {

	public int magic;

	public const int maxMagic = 100;

	private Resource.ResourceType favouredTribute;

	public int magicPerTribute;
	public int bonusMagicPerFavouredTribute;

	public DragonData data;

	public Castle castle;

	public List<Unit> activeUnits = new List<Unit>();
	public List<Settlement> alliedSettlements = new List<Settlement>();

	private bool isActive = false;

	public void Awake()
	{
		magic = 70;

	}

	public void Reset()
	{
		magic = 70;
		activeUnits.Clear();
		alliedSettlements.Clear();
	}

	public void ActivatePlayer()
	{
		//add message subscriptions here

		isActive = true;
	}

	public void DeactivatePlayer()
	{
		//remove message subscriptions here

		isActive = false;
	}

	public void ReceiveTribute(Resource res)
	{
		if (res.isTribute)
		{
			magic += res.value * magicPerTribute;

			if (res.type == favouredTribute)
				magic += res.value * bonusMagicPerFavouredTribute;

			magic = (magic > maxMagic) ? maxMagic : magic;
		}
	}

	/// <summary>
	/// Subtracts all maintenance costs for this turn - happens at the end of the turn
	/// Should only take for units that weren't created in this turn
	/// </summary>
	/// <returns>The magic.</returns>
	public void TakeMagicMaintenanceCosts()
	{
		foreach (Unit u in activeUnits)
		{
			magic -= u.unitData.maintenanceCost;
		}

		if (isActive)
		Game.instance.messageBroker.SendMessage<Dragon>(Messages.Events.MagicUsed, this);

	}

	/// <summary>
	/// Reset any flags necessary to begin the turn - e.g units are able to be destroyed until they have been moved
	/// </summary>
	/// <returns>The turn.</returns>
	public void BeginTurn()
	{
		foreach (Unit u in activeUnits)
		{
			u.hasMoved = false;
		}

	}

	public void OnUnitDestroy(Unit u)
	{
		if (activeUnits.Contains(u))
		{	
			//Debug.Assert(activeUnits.Contains(u));

			activeUnits.Remove(u);
			
			magic -= u.unitData.destructionCost;

			if (isActive)
			Game.instance.messageBroker.SendMessage<Dragon>(Messages.Events.MagicGained, this);
		}

	}

	public void OnUnitBuild(Unit u)
	{	
		Debug.Assert(!activeUnits.Contains(u));

		activeUnits.Add(u);
		magic -= u.unitData.creationCost;

		if (isActive)
		Game.instance.messageBroker.SendMessage<Dragon>(Messages.Events.MagicUsed, this);

	}

}
