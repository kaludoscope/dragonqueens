﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Player
{
	public Dragon dragon;
	public ID id ;

	public enum ID
	{
		None,
		Player1,
		Player2
	}


	public Player(Dragon _d, ID _i)
	{

		dragon = _d;
		id = _i;

	}


}

