# Dragon Queens

Hello! You've found the repository for the unfinished Unity game Dragon Queens, developed in 2017 by Kaludoscope (Delia Hamwood and Gemma Thomson), with 3D assets by Kate Holden.

The game isn't complete, but what's here is the codebase that was built during the time that Kaludoscope was running. It was intended to be robust and maintainable, so the focus is on architecture. What it lacks in polish it hopefully makes up for with clean messaging, useful tooling and separation of concerns.

The branch 'upgrade' has been recently upgraded to Unity 2019.4.18f1, and will run (although it isn't flashy). There's an error with some unit tests which require the dll for NSubstitute, which is currently failing to load, although this doesn't prevent the application itself from compiling.
